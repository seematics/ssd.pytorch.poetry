import numpy as np
from random import Random

from allegroai.utilities.annotation import get_class_id
from allegroai.utilities.augmentations import CustomAugmentationFixedCropSize
from allegroai.utilities.images import get_polygon_bounding_box
from future.utils import lmap

class CustomAugmentationFixedCropSizeWithProb(CustomAugmentationFixedCropSize):
    def __init__(self,  params=None, strength=None, arguments=None):
        self._random_box_prob = float(arguments.pop('random_box_prob', 0.0))
        super(CustomAugmentationFixedCropSizeWithProb, self).__init__(
            params=params,
            strength=strength,
            arguments=arguments
        )

    def should_draw_random_box(self, selected_box):
        should_draw_random = selected_box is None or np.random.rand() <= self._random_box_prob
        return should_draw_random


    def _calculate_frame_crop_from_annotated_polygons(self, im_shape, polygons, annotations):
        random = Random(self.params[0] if len(self.params) < 2 else self.params[1])

        def _choose_box(a_boxes, a_annotations):
            # check for valid class id (if exists), else use everything
            try:
                valid_indices = np.where(np.array(lmap(get_class_id, a_annotations)) >= 0)[0]
            except Exception:
                valid_indices = np.arange(len(a_annotations))
            if not valid_indices.size:
                return None
                # raise ValueError('invalid annotations (expected something mapped to a positive number)')
            random_index = valid_indices[random.randint(0, valid_indices.size - 1)]
            return np.atleast_2d(a_boxes[random_index]).astype(np.int)[0]

        def _clip_box_axis(axis_point_1, axis_point_2, axis_crop_box_len, axis_random_box_len, axis_img_len):
            offset = 0.5 * axis_random_box_len - 0.5 * axis_crop_box_len
            clipped_axis_point_1 = axis_point_1 + offset
            clipped_axis_point_2 = axis_point_2 - offset
            if clipped_axis_point_1 < 0:
                overflow = abs(clipped_axis_point_1)
                clipped_axis_point_1 = 0
                clipped_axis_point_2 = clipped_axis_point_2 + overflow
                if clipped_axis_point_2 > axis_img_len - 1:
                    clipped_axis_point_2 = axis_img_len - 1
            elif clipped_axis_point_2 > axis_img_len - 1:
                overflow = clipped_axis_point_2 - (axis_img_len - 1)
                clipped_axis_point_2 = axis_img_len - 1
                clipped_axis_point_1 = clipped_axis_point_1 - overflow
                if clipped_axis_point_1 < 0:
                    clipped_axis_point_1 = 0
            return clipped_axis_point_1, clipped_axis_point_2

        crop_height, crop_width = (self._fixed_height, self._fixed_width)
        # now ask for the boxes
        boxes = [get_polygon_bounding_box(poly) for poly in polygons]
        selected_box = _choose_box(boxes, annotations)

        if self.should_draw_random_box(selected_box):
            x1, y1 = [random.randint(0, im_shape[1]), random.randint(0, im_shape[0])]
            selected_box_width = 48 + random.randint(0, max(0, im_shape[1] - x1 - 48))
            selected_box_height = 48 + random.randint(0, max(0, im_shape[0] - y1 - 48))
            x2, y2 = [min(x1 + selected_box_width, im_shape[1] - 1), min(y1 + selected_box_height, im_shape[0] - 1)]
        else:
            x1, y1, x2, y2 = selected_box
            selected_box_width = x2 - x1
            selected_box_height = y2 - y1

        new_im_y1, new_im_y2 = _clip_box_axis(y1, y2, crop_height, selected_box_height, im_shape[0])
        new_im_x1, new_im_x2 = _clip_box_axis(x1, x2, crop_width, selected_box_width, im_shape[1])

        # TODO bypass augmenation if selected box is too similar to original box (return None here)
        return np.array([new_im_x1, new_im_y1, new_im_x2, new_im_y2], dtype=int)
