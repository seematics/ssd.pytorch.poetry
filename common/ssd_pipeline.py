from copy import deepcopy
from functools import partial
from typing import List
from common import get_uri_from_frame_source

import numpy as np
from collections import namedtuple

import torch
from allegroai import ImageFrame
from allegroai.debugging import get_null_logger
from allegroai.utilities.images import get_polygon_bounding_box

# HARD_LABEL_NAMES = {"hard", "__hard__", "difficult"}

ssd_input = namedtuple("SSDInput", ["images", "targets"])
ssd_batch = namedtuple(
    "SSDBatch",
    [
        "torch_input",
        "np_images",
        "ground_truth",
        "ground_truth_labels",
        "hard_ground_truth",
        "hard_ground_truth_labels",
        "original_metaframe"
    ],
)

roi_meta = namedtuple("RoiMeta", ["boxes", "labels", "label_names"])


def extract_roi_metadata(polygons):
    boxes = []
    labels = []
    label_names = []
    hard_boxes = []
    hard_labels = []
    hard_label_names = []
    if len(polygons):
        for roi in polygons:
            num = roi["labels"].get("id", -1)
            names = roi["labels"].get("name", ["BadLabel"])
            box = np.maximum(get_polygon_bounding_box(roi["points"]), 0).astype(
                dtype=np.float32
            )
            if num >= 0:
                labels.append(num)
                label_names.append(names)
                boxes.append(box)
            elif num == -2:
                hard_labels.append(num)
                hard_label_names.append(names)
                hard_boxes.append(box)
    return (
        roi_meta(boxes, labels, label_names),
        roi_meta(hard_boxes, hard_labels, hard_label_names),
    )


def make_ssd_get_item(
    target_width_height,
    num_input_channels=3,
    source_id=None,
    must_have_gt=False,
    image_dtype=np.float32,
    logger=None,
):
    logger = logger if logger is not None else get_null_logger()
    force_width, force_height = target_width_height
    box_normalization = np.array(
        [force_width, force_height, force_width, force_height, 1], dtype=np.float32
    )
    null_gt_item = np.empty([0, 5], dtype=np.float32)

    def ssd_get_item(image_frame: ImageFrame):

        polygons = image_frame.get_polygons()
        meta = image_frame.get_meta_data()

        if must_have_gt and not len(polygons):
            if hasattr(logger, "warn"):
                logger.warn("(Frame drop) No ground truth for image {}".
                            format(get_uri_from_frame_source(meta, source_id=source_id)))
            return None

        image_data = image_frame.get_data(source_id=source_id)

        if image_data is None:
            if hasattr(logger, "warn"):
                logger.warn("(Frame drop) Problem loading image {}".
                            format(get_uri_from_frame_source(meta, source_id=source_id)))

        roi_meta, hard_roi_meta = extract_roi_metadata(polygons)

        if len(roi_meta.boxes) == 0 and must_have_gt:
            # all rois were dropped
            if hasattr(logger, "warn"):
                logger.warn(
                    "(Frame drop) All rois dropped after processing in {}".format(
                        get_uri_from_frame_source(meta, source_id=source_id))
                )
            return None

        h, w, num_channels = image_data.shape

        orig_w, orig_h = image_frame.get_original_size(source_id=source_id)
        meta.loaded_source_original_size_wh = (orig_w, orig_h)
        meta.loaded_source_resized_size_wh = (w, h)

        #                           rgb <-> bgr     h,w,c -> c, h ,w
        # image_data = image_data[:, :, (2, 1, 0)].transpose(2, 0, 1)
        image_data = image_data.transpose(2, 0, 1)
        # put image inside zero tensor of the appropriate size
        if h != force_height or w != force_width or num_channels != num_input_channels:
            image = np.zeros((num_channels, force_height, force_width), dtype=image_dtype)
            image[:num_channels, :h, :w] = image_data.astype(image_dtype)
        else:
            image = image_data.astype(image_dtype)
        # wrap with torch
        image = torch.from_numpy(image)

        gt_rois_labels = null_gt_item
        hard_gt_rois_labels = null_gt_item
        if len(roi_meta.boxes):
            boxes = np.array(roi_meta.boxes)
            gt_rois_labels = np.hstack((boxes, np.expand_dims(roi_meta.labels, axis=1)))

        if len(hard_roi_meta.boxes):
            hard_boxes = np.array(hard_roi_meta.boxes)
            hard_gt_rois_labels = np.hstack(
                (hard_boxes, np.expand_dims(hard_roi_meta.labels, axis=1))
            )

        target = torch.FloatTensor(gt_rois_labels/box_normalization)

        # extra safety
        image_frame.release_data()

        return ssd_batch(
            torch_input=ssd_input(images=image, targets=target),
            np_images=None,
            ground_truth=gt_rois_labels,
            ground_truth_labels=roi_meta.label_names,
            hard_ground_truth=hard_gt_rois_labels,
            hard_ground_truth_labels=hard_roi_meta.label_names,
            original_metaframe=meta,
        )

    return ssd_get_item


def make_ssd_batcher(cuda_on=True):
    null_batch_gt_item = np.empty([0, 6])

    def stack_and_add_batch_id(data: List[np.ndarray]):
        if not len(data):
            return null_batch_gt_item
        stackable = []
        for batch_id, array in enumerate(data):
            batch_idx = np.full((len(array), 1), fill_value=batch_id)
            stackable.append(np.hstack((batch_idx, np.array(array))))
        return np.vstack(stackable)

    def ssd_batcher(ssd_items: List[ssd_batch], cuda=cuda_on):

        images = torch.stack(tuple(ssd.torch_input.images for ssd in ssd_items))
        targets = [ssd.torch_input.targets for ssd in ssd_items]

        device = torch.cuda.current_device() if cuda else torch.device('cpu')

        np_images = images.numpy()
        # move tensors to device:
        images = images.to(device)
        with torch.no_grad():
            targets = [ann.to(device) for ann in targets]

        ground_truth = stack_and_add_batch_id([ssd.ground_truth for ssd in ssd_items])
        ground_truth_labels = [ssd.ground_truth_labels for ssd in ssd_items]
        hard_ground_truth = stack_and_add_batch_id(
            [ssd.hard_ground_truth for ssd in ssd_items]
        )
        hard_ground_truth_labels = [ssd.hard_ground_truth_labels for ssd in ssd_items]
        orig_meta = [ssd.original_metaframe for ssd in ssd_items]

        return ssd_batch(
            torch_input=ssd_input(images=images, targets=targets),
            np_images=np_images,
            ground_truth=ground_truth,
            ground_truth_labels=ground_truth_labels,
            hard_ground_truth=hard_ground_truth,
            hard_ground_truth_labels=hard_ground_truth_labels,
            original_metaframe=orig_meta,
        )

    return partial(ssd_batcher, cuda=cuda_on)
