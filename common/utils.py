import copy
import logging
import os
import argparse
import random
import numpy as np

import threading
from collections import namedtuple
from tempfile import mkstemp

import torch
from allegroai.debugging.log import LoggerRoot
from allegroai.utilities.images import ResizeStrategy
from pathlib2 import Path
from torch.optim.lr_scheduler import _LRScheduler

from allegroai import ImageFrame, DatasetVersion

from models.ssd_resnet import build_ssd_with_resnet
from models.ssd_vgg import build_ssd_with_vgg
from models.ssd_squeezenet import build_ssd_with_squeezenet
from models.ssd_mobilenetv2 import build_ssd_with_mobilenetv2

from layers import Detect, DragonLoss

ImageSizeTuple = namedtuple('ImageSizeTuple', 'w h')


def nan_safe_item(zero_d_tensor):
    item = zero_d_tensor.item()
    if np.isinf(item):
        return 0
    return np.nan_to_num(item)


def ssd_inference_on_image(net, single_image, cuda_is_on, ssd_config_params):
    # DEPRECATED - will be removed soon
    single_image = torch.from_numpy(single_image)
    with torch.no_grad():
        in_image = single_image.cuda() if cuda_is_on else single_image

    # forward
    detections = net.data_parallel(in_image)
    detections = detections.data.cpu().numpy()
    all_image_boxes, all_image_scores = \
        ssd_output_to_allegro_format(numpy_ssd_output=detections,
                                     conf_thresh=ssd_config_params['test_detection_conf_thresh'],
                                     img_width=ssd_config_params['min_dim_w'],
                                     img_height=ssd_config_params['min_dim_h'])
    return all_image_boxes, all_image_scores


def ssd_inference_on_tensor(image_tensor, net, conf_thresh, target_image_size_wh):
    if len(image_tensor.shape) == 3:
        image_tensor = image_tensor.unsqueeze(0)
    with torch.no_grad():
        detections = net(image_tensor, conf_thresh=conf_thresh)
    detections = detections.data.cpu().numpy()
    all_image_boxes, all_image_scores = \
        ssd_output_to_allegro_format(numpy_ssd_output=detections,
                                     conf_thresh=conf_thresh,
                                     img_width=target_image_size_wh[0],
                                     img_height=target_image_size_wh[1],
                                     )
    return all_image_boxes, all_image_scores


def make_deterministic(seed):
    # setup radnom seeds
    # cudnn.deterministic = True
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)


def build_ssd(args, config_params, variant, target_image_size, logger=None):
    if 'dropout' not in vars(args):
        args.dropout = 0.1
    if args.feature_extraction_type == 'vgg16':
        freeze_n = 21
        ssd_net = build_ssd_with_vgg(cfg=config_params, phase='train',
                                     variant=variant, size=target_image_size,
                                     num_classes=config_params['num_classes'], vgg_type='vgg16',
                                     dropout=args.dropout, logger=logger)
    elif args.feature_extraction_type == 'vgg19':
        freeze_n = 25
        ssd_net = build_ssd_with_vgg(cfg=config_params, phase='train',
                                     variant=variant, size=target_image_size,
                                     num_classes=config_params['num_classes'], vgg_type='vgg19',
                                     dropout=args.dropout, logger=logger)
    elif args.feature_extraction_type == 'squeezenet1_0':
        freeze_n = 10
        ssd_net = build_ssd_with_squeezenet(cfg=config_params, phase='train',
                                            variant=variant, size=target_image_size,
                                            num_classes=config_params['num_classes'], version=1.0,
                                            dropout=args.dropout, logger=logger)
    elif args.feature_extraction_type == 'squeezenet1_1':
        freeze_n = 7
        ssd_net = build_ssd_with_squeezenet(cfg=config_params, phase='train',
                                            variant=variant, size=target_image_size,
                                            num_classes=config_params['num_classes'], version=1.1,
                                            dropout=args.dropout, logger=logger)
    elif args.feature_extraction_type == 'resnet50':
        freeze_n = 16
        ssd_net = build_ssd_with_resnet(cfg=config_params, phase='train',
                                        variant=variant, size=target_image_size,
                                        num_classes=config_params['num_classes'], resnet_type='resnet50',
                                        logger=logger)
    elif args.feature_extraction_type == 'resnet101':
        freeze_n = 33
        ssd_net = build_ssd_with_resnet(cfg=config_params, phase='train',
                                        variant=variant, size=target_image_size,
                                        num_classes=config_params['num_classes'], resnet_type='resnet101',
                                        logger=logger)
    elif args.feature_extraction_type.lower() == 'mobilenetv2':
        mobilenet_params = dict(width_mult=1.0)
        freeze_n = 13
        ssd_net = build_ssd_with_mobilenetv2(cfg=config_params, phase='train',
                                             variant=variant, size=target_image_size,
                                             num_classes=config_params['num_classes'],
                                             mobilenet_params=mobilenet_params,
                                             logger=logger)

    else:
        freeze_n = 0
        raise ValueError('Unsupported feature extraction type {}'.format(args.feature_extraction_type))

    return ssd_net, freeze_n


def bound_number_type(minimum=None, maximum=None, dtype=float):
    """
    bound_number_type

    Creates a bounded integer "type" (validator function)
    for use with argparse.ArgumentParser.add_argument.
    At least one of ``minimum`` and ``maximum`` must be passed.

    :param minimum: maximum allowed value
    :param maximum: minimum allowed value
    :param dtype: argument type (either float or int)
    """

    if minimum is maximum is None:
        raise ValueError('either "minimum" or "maximum" must be provided')

    def bound_value(arg):
        num = dtype(arg)

        if minimum is not None and num < minimum:
            raise argparse.ArgumentTypeError('minimum value is {}'.format(minimum))
        if maximum is not None and num > maximum:
            raise argparse.ArgumentTypeError('maximum value is {}'.format(minimum))
        return num

    return bound_value


def get_model_url(feature_extraction_type, start_from_model_zoo=False):
    if start_from_model_zoo:
        # using pure model zoo:
        feature_extraction_type = feature_extraction_type.lower()
        if feature_extraction_type == 'vgg16':
            url = 'https://download.pytorch.org/models/vgg16-397923af.pth'
            name = 'torchvision model zoo VGG16'
        elif feature_extraction_type == 'vgg19':
            url = 'https://download.pytorch.org/models/vgg19-dcbb9e9d.pth'
            name = 'torchvision model zoo VGG19'
        elif feature_extraction_type == 'squeezenet1_0':
            url = 'https://download.pytorch.org/models/squeezenet1_0-a815701f.pth'
            name = 'torchvision model zoo SqueezeNet version 1.0'
        elif feature_extraction_type == 'squeezenet1_1':
            url = 'https://download.pytorch.org/models/squeezenet1_1-f364aa15.pth'
            name = 'torchvision model zoo SqueezeNet version 1.1'
        elif feature_extraction_type == 'resnet50':
            url = 'https://download.pytorch.org/models/resnet50-19c8e357.pth'
            name = 'torchvision model zoo ResNet50'
        elif feature_extraction_type == 'resnet101':
            url = 'https://download.pytorch.org/models/resnet101-5d3b4d8f.pth'
            name = 'torchvision model zoo ResNet101'
        elif feature_extraction_type == 'mobilenetv2':
            url =  'https://download.pytorch.org/models/mobilenet_v2-b0353104.pth'
            name = 'torchvision model zoo MobileNetV2'

        else:
            raise ValueError('Feature extraction type can be only one of the following: mobilenetv2, '
                             'vgg16, vgg19, squeezenet1_0,squeezenet1_1, resnet50 and resnet101')
    else:
        if feature_extraction_type == 'vgg16':
            # url = 'https://s3.amazonaws.com/allegro-models/Pytorch_SSD_VGG16_trained_on_VOC2012.pth'
            # name = 'Pytorch SSD | feature extraction - torchvision model zoo VGG16 | classifier - trained on VOC'
            url = 'https://s3.amazonaws.com/allegro-models/Pytorch_SSD_person_detector_VGG16.pth'
            name = 'Pytorch SSD person detector VGG16'
        elif feature_extraction_type == 'vgg19':
            # 'https://download.pytorch.org/models/vgg19-dcbb9e9d.pth'
            url = 'https://s3.amazonaws.com/allegro-models/Pytorch_SSD_VGG19_trained_on_VOC2012.pth'
            name = 'Pytorch SSD | feature extraction - torchvision model zoo VGG19 | classifier - trained on VOC'
        elif feature_extraction_type == 'squeezenet1_0':
            # 'https://download.pytorch.org/models/squeezenet1_0-a815701f.pth'
            url = 'https://s3.amazonaws.com/allegro-models/Pytorch_SSD_SqueezeNet1_0_trained_on_VOC2012.pth'
            name = 'Pytorch SSD | feature extraction - torchvision model zoo SqueezeNet 1.0 |' \
                   ' classifier - trained on VOC'
        elif feature_extraction_type == 'squeezenet1_1':
            url = 'https://s3.amazonaws.com/allegro-models/Pytorch_SSD_SqueezeNet1_1_trained_on_KITTI.pth'
            name = 'torchvision model zoo SqueezeNet version 1.1'
        elif feature_extraction_type == 'resnet50':
            url = 'https://s3.amazonaws.com/allegro-models/Pytorch_SSD_person_detector_ResNet50.pth'
            name = 'Pytorch SSD person detector ResNet50'
        elif feature_extraction_type == 'resnet101':
            url = 'https://s3.amazonaws.com/allegro-models/Pytorch_SSD_person_detector_ResNet101.pth'
            name = 'Pytorch SSD person detector ResNet101'
        elif feature_extraction_type == 'mobilenetv2':
            # TODO - pretrained model here
            url = 's3://allegro-models/Pytorch_mobilenet_v2_tonylins.pth'
            name = 'trained by github user tonlyins with reported accuracy'

        else:
            raise ValueError('Feature extraction type can be only one of the following: mobilenetv2, '
                             'vgg16, vgg19, squeezenet1_0,squeezenet1_1, resnet50 and resnet101')

    return url, name


def check_labels(labels):
    # Sanity check for the final labels:
    positive_model_labels = [v for _, v in labels.items() if v > 0]
    model_num_positive_classes = len(positive_model_labels)
    if not model_num_positive_classes:
        raise ValueError('No positive label id defined for the model. Current label enumeration : %s' % str(labels))
    elif model_num_positive_classes != max(positive_model_labels):
        raise ValueError('Not all model labels have a defined id, cannot run task. num id >0 : '
                         '%d while largest id is %d' % (model_num_positive_classes, max(positive_model_labels)))
    return True


def adj_mean_of_image_batch(images, bgr_means):
    bgr_means = np.array(bgr_means)
    images += bgr_means
    return images


def freeze_first_n_layers(nn_module, n=21):
    for i, param in enumerate(nn_module.parameters()):
        if i < n:
            param.requires_grad = False
        if i > n:
            break


def prior_output_to_allegro_format(numpy_prior_output, img_width, img_height):
    """
    Helper function to generate output that fits the usual debug images creation
    mainly it resizes the priors back to image coordinates, and returns a fake score

    :param numpy_prior_output:
    :param img_width:
    :param img_height:
    :return:
    """
    all_batch_boxes = None

    # resizer = np.array([img_height, img_width, img_height, img_width])
    resizer = np.array([img_width, img_height, img_width, img_height])

    if numpy_prior_output is not None and len(numpy_prior_output):
        numpy_prior_output[:, 1:5] *= resizer
        all_batch_boxes = np.around(numpy_prior_output).astype(np.int)

    all_batch_fake_scores = np.ones(len(all_batch_boxes), dtype=all_batch_boxes.dtype)\
        if all_batch_boxes is not None else None

    return all_batch_boxes, all_batch_fake_scores


def ssd_output_to_allegro_format(numpy_ssd_output, conf_thresh, img_width, img_height):
    """
    Transform ssd output to Seematics format of bounding boxes
    Providing None as img width and height will return relative coordinates for the boxes
    :param numpy_ssd_output: (batch_size, num_classes, num_boxes, 5) - each v \in R^5 is (cls_score, x1, y1, x2, y2)
                             we have a v for every box class and batch images
    :param conf_thresh: minimum confidence for transformation
    :param img_width: original image width (x1y1x2y2 boxes are in relative coords)
    :param img_height: original image height (x1y1x2y2 boxes are in relative coords)

    :return:
        all_batch_boxes - (num_boxes, 5) wehere each v \in R^5 is (batch_id, x1, y1, x2, y2, label)
        all_batch_scores - (num_boxes, ) of scores
    """

    all_batch_boxes = []
    all_batch_scores = []

    for bidx in range(numpy_ssd_output.shape[0]):
        cls_id, box_id = np.where(numpy_ssd_output[bidx, :, :, 0] > conf_thresh)
        b_dets = numpy_ssd_output[bidx, cls_id, box_id, :]
        b_dets = np.minimum(1., np.maximum(0., b_dets))
        if not b_dets.size:
            continue
        b_dets = np.hstack([np.ones((b_dets.shape[0], 1)) * bidx, b_dets])
        b_dets = np.atleast_2d(b_dets)[:, [0, 2, 3, 4, 5, 1]]

        if img_width is not None and img_height is not None:
            b_dets[:, 1:5:2] *= img_width
            b_dets[:, 2:5:2] *= img_height

        scores = b_dets[:, 5]
        pred_boxes = np.hstack([b_dets[:, :5], cls_id[:, np.newaxis]])
        all_batch_boxes.append(pred_boxes)
        all_batch_scores.append(scores)

    all_batch_boxes = np.vstack(all_batch_boxes) if len(all_batch_boxes) else None
    all_batch_scores = np.concatenate(all_batch_scores) if len(all_batch_scores) else None

    return all_batch_boxes, all_batch_scores


class CyclicLRScheduler(_LRScheduler):

    def __init__(self, optimizer, upper_bound_steps, lr_list, last_epoch=-1):
        self._total_cycle_len = sum(upper_bound_steps)
        self._upper_bounds = upper_bound_steps
        self._lrs = lr_list
        self._lr_length = len(optimizer.param_groups)
        self._buckets = copy.copy(upper_bound_steps)
        self._bucket_index = 0
        super(CyclicLRScheduler, self).__init__(optimizer=optimizer, last_epoch=last_epoch)

    def get_lr(self):
        cur_bucket_size = self._buckets[self._bucket_index]
        if cur_bucket_size < 1:
            self._bucket_index += 1
            if self._bucket_index == len(self._upper_bounds):
                self._bucket_index = 0
                self._buckets = copy.copy(self._upper_bounds)

        self._buckets[self._bucket_index] -= 1

        return [self._lrs[self._bucket_index]] * self._lr_length


def setup_pytorch(args, logger=None, use_cudnn=True, suppress_warnings=False):
    """
    :param suppress_warnings: if running on pytorch 0.4 you probably would want to suppress warnings
    :param args: ArgParser object
    :param logger: logger object (for console output)
    :param use_cudnn: True to use cuda, False for cpu mode
    :return: cuda state (True if on)
    """
    cuda_state_from_args = args.cuda if hasattr(args, 'cuda') else use_cudnn
    if cuda_state_from_args and torch.cuda.is_available():
        if use_cudnn:
            torch.set_default_tensor_type('torch.cuda.FloatTensor')
        else:
            logger.console("It looks like you have a CUDA device, but aren't " +
                           "using CUDA.\nRun with --cuda for optimal training speed.", level=logging.WARNING)
            torch.set_default_tensor_type('torch.FloatTensor')
            use_cudnn = False
    else:
        torch.set_default_tensor_type('torch.FloatTensor')
        if torch.cuda.is_available():
            logger.console("Not using GPU for running pytorch", level=logging.WARNING)
        use_cudnn = False

    if use_cudnn:
        cudnn_version = torch.backends.cudnn.version()
        if cudnn_version:
            logger.console('Using CUDNN version %s' % cudnn_version)
            torch.backends.cudnn.benchmark = True
            logger.console('Enabled optimization for fixed input size')
        else:
            logger.console('CUDNN backend not available!', level=logging.WARNING)

    if suppress_warnings:
        import warnings
        warnings.filterwarnings(action="ignore", message="volatile.*")

    return use_cudnn


def yaml_file_to_text(path_to_yaml):
    p = Path(path_to_yaml)
    return p.open('r').read()


def upload_model_snapshot(net, model, iteration):
    fd, filename = mkstemp(suffix=('.%d.pth' % iteration))
    torch.save(net.state_dict(), open(filename, 'wb'))
    os.close(fd)
    # assert model.is_output_model, "only output models can be updated"
    model.update_weights(filename)


def normalize_boxes(boxes, img_w, img_h):
    # Assumes batch id is the first element of roi
    boxes = np.array(boxes)
    assert boxes.shape[1] == 4, 'Expect boxes of shape (N, 4)'
    boxes[:, ::2] /= float(img_w)
    boxes[:, 1::2] /= float(img_h)
    return boxes.astype(np.float32)


def jitter_box_and_score(box, score, crop=0.6, jitter_span=0.):
    crop /= 2.
    dx, dy = ((box[2:] - box[:2]) * crop).astype(np.int)
    dd = int((2 * np.random.rand() - 1) * min(dx, dy) * jitter_span)
    box += [dx, dy, -dx, -dy]
    box += dd
    return box, score


def resolve_resize_strategy(parsed_args, logger):
    parsed_dict = vars(parsed_args)
    strategies = {
        'always': ResizeStrategy.RESIZE_ALWAYS,
        'keep_ar': ResizeStrategy.ALWAYS_KEEP_ASPECT_RATIO,
        'bigger_keep_ar': ResizeStrategy.BIGGER_KEEP_ASPECT_RATIO,
        'default_for_ssd': ResizeStrategy.BIGGER_KEEP_ASPECT_RATIO
    }
    if 'resize_strategy' in parsed_dict:
        my_strat = parsed_dict['resize_strategy']
        if my_strat not in strategies:
            logger.warn('"{}" not in supported strategies: {}, using DEFAULT.'.
                        format(my_strat, [str(k) for k in strategies.keys()]))
        return strategies.get(my_strat, strategies['default_for_ssd'])
    else:
        # backwards compatibility
        return ResizeStrategy.BIGGER_KEEP_ASPECT_RATIO


class KeepaliveHandler(logging.Handler):
    def __init__(self, default_message, logger, interval=20.0, level=logging.NOTSET):
        super(KeepaliveHandler, self).__init__(level)
        self.interval = interval
        self._message = default_message
        self.logger = logger
        self.timer = None
        self.should_stop = False

        # temporary safeguard for hanging thread messing up in dev mode
        self._max_fire_before_message_changed = 3600 // interval
        self._num_fired = 0

        self._reset()

    @property
    def message(self):
        return self._message

    @message.setter
    def message(self, message):
        self._message = message
        self._num_fired = 0

    def _reset(self):
        if self.timer:
            self.timer.cancel()
        if not (self.should_stop or self._num_fired > self._max_fire_before_message_changed):
            self.timer = threading.Timer(self.interval, self._fire)
            self.timer.start()

    def _fire(self):
        try:
            self.logger.console(self._message)
            self._num_fired += 1
        except Exception:
            pass
        self._reset()

    def emit(self, record):
        self._reset()


def setup_keepalive_logger(default_message, logger, interval=120.0):
    handler = KeepaliveHandler(default_message, logger, interval)
    LoggerRoot.get_base_logger().addHandler(handler)
    return handler


def version_exists(dataset_name: str, version_name: str):
    try:
        DatasetVersion.get_version(
            dataset_name=dataset_name, version_name=version_name
        )
        return True
    except ValueError:
        return False

