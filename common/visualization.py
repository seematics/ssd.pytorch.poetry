import numpy as np

from allegroai.metrics import DebugImagesExtender
from allegroai.utilities.visualization import getcolormap


class DrawBoxesAndLabels(DebugImagesExtender):
    """
    Helper to draw ground truth and predicted boxes on an image
    """

    def __init__(self, labels_mapping, logger, ground_truth_label_mapping_cls_to_id=None, **kwargs):
        """
        :param labels_mapping: a dictionary mapping (label number) -> (lable string)
        :param logger: Seematics logger
        """
        super(DrawBoxesAndLabels, self).__init__(**kwargs)
        self._labels_mapping = labels_mapping
        if ground_truth_label_mapping_cls_to_id is None:
            # reverse mapping of id_to_cls name
            ground_truth_label_mapping_cls_to_id = {v: k for k, v in labels_mapping.items()}
        self._ground_truth_label_mapping_cls_to_id = ground_truth_label_mapping_cls_to_id
        custom_colormap = kwargs.pop('colormap', None)
        self._colormap = custom_colormap or getcolormap(len(labels_mapping.keys()))
        # self._alpha_value = 0.5
        # self._gt_rect_thickness = 1
        # self._thickness = 1
        # self._add_legend = True
        self._log = logger

    def __call__(self, images, gt_boxes=None, pred_boxes=None, pred_scores=None,
                 gt_boxes_labels=None, gt_boxes_hard=None, gt_boxes_hard_labels=None):
        """
        :param images: np.arary of shape (batch_size, img height, img width, color channels)
        :param gt_boxes: np.array of shape (num boxes, 6) where the 5 is [batch idx, x1, y1, x2, y2, label]
        :param pred_boxes:  np.array of shape (num boxes, 6) where the 5 is [batch idx, x1, y1, x2, y2, label]
        :param pred_scores: np.array of shape (num boxes, ) A 1D array of float scores \in (0, 1)
        # TODO - fix docstring
        :return:
        """
        if pred_boxes is not None and pred_scores is None:
            raise ValueError('pred_scores must be provided if pred_boxes is provided')
        self.update_data(gt_boxes=gt_boxes, pred_boxes=pred_boxes, pred_scores=pred_scores, images=images,
                         gt_boxes_labels=gt_boxes_labels, gt_boxes_hard=gt_boxes_hard,
                         gt_boxes_hard_labels=gt_boxes_hard_labels)
        return self.draw_on_images()

    def update_data(self, gt_boxes, pred_boxes, pred_scores, images,
                    gt_boxes_labels=None, gt_boxes_hard=None, gt_boxes_hard_labels=None):
        if pred_boxes is not None and pred_boxes.size:
            if self._score_threshold is not None and self._score_threshold > 0:
                above_thresh_indexes = np.where(pred_scores >= self._score_threshold),
                self._pred_scores = pred_scores[above_thresh_indexes]
                self._pred_labels = pred_boxes[above_thresh_indexes, 5]
                self._pred_boxes = pred_boxes[above_thresh_indexes, 1:5]
                self._pred_batch_numbers = pred_boxes[above_thresh_indexes, 0]
            else:
                self._pred_scores = pred_scores
                self._pred_labels = pred_boxes[:, 5]
                self._pred_boxes = pred_boxes[:, 1:5]
                self._pred_batch_numbers = pred_boxes[:, 0]
        else:
            self._pred_scores = None
            self._pred_labels = None
            self._pred_boxes = None
            self._pred_batch_numbers = None

        if gt_boxes is not None and gt_boxes.size:
            self._gt_labels = gt_boxes[:, 5]
            self._gt_boxes = gt_boxes[:, 1:5]
            self._gt_batch_numbers = gt_boxes[:, 0]
        else:
            self._gt_labels = None
            self._gt_boxes = None
            self._gt_batch_numbers = None

        self._gt_boxes_labels = gt_boxes_labels
        self._gt_boxes_hard = gt_boxes_hard if gt_boxes_hard is not None and len(gt_boxes_hard) else None
        self._gt_boxes_hard_labels = gt_boxes_hard_labels if \
            gt_boxes_hard_labels is not None and len(gt_boxes_hard_labels) else None

        self._images = list(images)

    def draw_on_images(self):
        labels_mapping = self._labels_mapping
        self._images = [self._add_gt_to_image(n, alpha_value=self._alpha_value) for n in range(len(self._images))]
        # draw hard predictions
        if self._gt_boxes_hard is not None:
            for i, image in enumerate(self._images):
                hard_boxes_idx, = np.where(self._gt_boxes_hard[:,0] == i)
                if len(hard_boxes_idx):
                    hard_boxes = self._gt_boxes_hard[hard_boxes_idx, 1:5]
                    hard_clr = [(0, 254, 0)] * len(hard_boxes)
                    image = self._draw_shaded_rect_on_image(image, hard_boxes, hard_clr, alpha_value=0.2)
                    self._images[i] = self._draw_colored_rect(image, hard_boxes, hard_clr, thickness=2, style='dashed')

        # Draw predictions
        self._images = [self._add_pred_to_image(n) for n in range(len(self._images))]

        if self._add_legend:
            if self._gt_boxes is not None and self._gt_boxes.size != 0:
                self._legend_labels = [set().union(self._gt_labels[self._gt_batch_numbers == n].tolist()) for n in
                                       range(len(self._images))]
            else:
                self._legend_labels = [[] for n in range(len(self._images))]

            if self._pred_boxes is not None and self._pred_boxes.size != 0:
                self._legend_labels = [
                    (self._legend_labels[n] or set()).union(self._pred_labels[self._pred_batch_numbers == n].tolist())
                    for n in
                    range(len(self._images))]
            self._images = [self._add_legend_to_image(self._images[n], self._legend_labels[n], labels_mapping) for n in
                            range(len(self._images))]

        return [img.copy() for img in self._images]
