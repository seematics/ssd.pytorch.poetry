import numpy as np
from collections import OrderedDict
from pathlib import Path

import torch
import pandas
import yaml

from allegroai import DataView, Task, DataPipe, ImageFrame
from allegroai.debugging import get_null_logger
from allegroai.task_parameters import param, TaskParameters
from allegroai.utilities.datapipes import ScanningDataPipe
from matplotlib.collections import PatchCollection
from tqdm import tqdm

from common.prior_optimization import AlignTypeEnum, BoxAlignCalculator, create_default_vis_options
from common.utils import ImageSizeTuple, build_ssd

DEBUG = False
# DEBUG = True
if DEBUG:
    from matplotlib import pyplot as plt, patches as patches
    plt.style.use('seaborn-talk')


class ClusteringTaskParameters(TaskParameters):
    n_samples = param(type=int, range=[10, None], desc='Number of samples to draw upon from dataview for analysis')
    cluster_threshold = param(type=float, range=[0, 1], desc='Used to threshold cluster importance')
    max_n_clusters = param(type=int, range=[1, 15], desc='Max Number of clusters to create from drawn samples')
    min_n_clusters = param(type=int, range=[1, 15], desc='Min Number of clusters to create from drawn samples')
    resize_w = param(type=int, range=[100, None], desc='Limit the width of images to max this value in pixels')
    resize_h = param(type=int, range=[100, None], desc='Limit the height of images to max this value in pixels')
    upload_destination = param(type=str, desc='Bucket to upload debug images to')
    feature_extraction_type = param(type=str, desc='type of ssd net to generate, determines number of priors')


def create_new_cfg_with_priors(priors_guess_df, image_size, task_params, cfg=None, logger=None):
    logger = logger if logger else Task.current_task().get_logger()
    if cfg is None:
        default_design_file = Path(__file__).parents[1] / Path('ssd_design.yaml')
        if not default_design_file.exists():
            raise IOError('Missing default model design file in repo: {}'.format(default_design_file))
        cfg = yaml.load(default_design_file.read_text())

    new_cfg = cfg.copy()
    # make sure the cfg is new format by using it to create priors - this converts the cfg to new style
    ssd_net = generate_net_with_priors(image_size, new_cfg, task_params, logger=None)

    # new cfg already has this but we do it explicitly here
    prior_design = ssd_net.priors_info
    for mg in prior_design.keys():
        priors = priors_guess_df[priors_guess_df['match_group'] == mg][['width', 'height']].values
        prior_design[mg]['basis'] = [np.around(box).astype(np.int).tolist() for box in priors]
        if 'num_priors' in prior_design[mg]:
            prior_design[mg].pop('num_priors')
        if 'feature_sizes_wh' in prior_design[mg]:
            prior_design[mg].pop('feature_sizes_wh')
    new_cfg['prior_design'] = prior_design

    # make sure this works
    try:
        sanity_cfg = new_cfg.copy()
        ssd_net = generate_net_with_priors(image_size, sanity_cfg, task_params, logger=None)
    except Exception:
        logger.error("Could not build priors that match the cfg?")
    return new_cfg


def filter_outliers(feature_sizes, gt_bbox_df, inplace=True,
                    drop_too_big=False, drop_too_small=False, logger=None):
    logger = logger if hasattr(logger,'warn') else get_null_logger()
    minimum_object_scale, maximum_object_scale = get_min_max_object_scales_from_feature_size_array(feature_sizes)
    s_0 = (gt_bbox_df['width'] < minimum_object_scale) & (gt_bbox_df['height'] < minimum_object_scale)
    s_inf = (gt_bbox_df['width'] > maximum_object_scale) | (gt_bbox_df['height'] > maximum_object_scale)
    excluded = gt_bbox_df['width'] < 0
    if any(s_0):
        logger.warn("There are {s0} objects smaller than the minimum object scale".format(s0=sum(s_0)))
        if drop_too_small:
            excluded |= s_0
    if any(s_inf):
        logger.warn("There are {sinf} objects with at least one spatial dimension"
                    " larger than the maximum possible scale to detect".format(sinf=sum(s_inf)))
        if drop_too_big:
            excluded |= s_inf
        logger.warn("removing objects from dataframe")

    if inplace:
        gt_bbox_df.drop(gt_bbox_df[excluded].index, inplace=True)
    else:
        gt_bbox_df = gt_bbox_df.drop(gt_bbox_df[excluded].index, inplace=False)
    return gt_bbox_df


def get_min_max_object_scales_from_feature_size_array(feature_sizes):
    feature_size_to_matcher = feature_sizes.min(axis=1)
    minimum_object_scale = feature_size_to_matcher.min()
    maximum_object_scale = feature_size_to_matcher.max()
    return int(minimum_object_scale), int(maximum_object_scale)


def create_match_scales_logic(feature_sizes, gt_bbox_df, logger=None):
    logger = logger if hasattr(logger,'warn') else get_null_logger()
    MAX_AR = 5

    if DEBUG:
        plt.figure()

    feature_size_to_matcher = feature_sizes.min(axis=1)
    minimum_object_scale = feature_size_to_matcher.min()
    # TODO - missing resolution - should be 7 and looking at 6
    min_sizes = feature_size_to_matcher[:-1]
    max_sizes = feature_size_to_matcher[1:]
    # fuzzy boundaries mkI
    # TODO - calculate the shared boundary sizes
    # min_sizes = [ms - minimum_object_scale for ms in min_sizes]
    # max_sizes = [ms + minimum_object_scale for ms in max_sizes]
    min_sizes = [max(ms, minimum_object_scale) for ms in min_sizes]
    max_sizes = [max(ms, minimum_object_scale) for ms in max_sizes]
    # split last range
    max_size_last = max_sizes[-1]
    min_sizes.append(min_sizes[-1] + (max_sizes[-1] - min_sizes[-1]) // 2)
    max_sizes[-1] = min_sizes[-1]
    max_sizes.append(max_size_last)
    low_bound = []
    for l_bound, r_bound in zip(min_sizes, max_sizes):
        w_bound = gt_bbox_df['width'].between(l_bound, r_bound, inclusive=True) & \
                  gt_bbox_df['height'].between(r_bound / MAX_AR, r_bound, inclusive=True)
        h_bound = gt_bbox_df['height'].between(l_bound, r_bound, inclusive=True) & \
                  gt_bbox_df['width'].between(l_bound / MAX_AR, r_bound, inclusive=True)
        low_bound.append(w_bound | h_bound)

        if DEBUG:
            plotme = gt_bbox_df[low_bound[-1]][['width', 'height']].values
            plt.scatter(*plotme.T)

    # extra debug - show outliers
    # if DEBUG:
    #     matched = low_bound[0].copy()
    #     for bound in low_bound:
    #         matched |= bound
    #     unmatched = ~matched
    #     values = gt_bbox_df[unmatched][['width', 'height']].values
    #     plt.scatter(*values.T, marker='x')

        # plt.show()

    return low_bound


def generate_net_with_priors(image_size, ssd_config, task_params, logger):
    # update size in config (if needed)
    dimkey = 'min_dim_' if 'min_dim_w' in ssd_config else 'input_dim_'
    ssd_config[dimkey + 'w'] = image_size.w
    ssd_config[dimkey + 'h'] = image_size.h
    net_variant = 512 if max(image_size) >= 400 else 300
    if logger is not None:
        logger.console('Constructing the SSD net...')
    ssd_net, freeze_n = build_ssd(task_params, ssd_config, net_variant, image_size, logger=logger)
    fake_image = torch.zeros([1, 3, image_size.h, image_size.w], dtype=torch.float32)
    ssd_net.generate_priors(
        feature_sizes_wh=ssd_net.get_multiscale_feature_sizes(
            input_image=fake_image,
            bypass_source_switch=True))
    return ssd_net


def emit_prior_wh_basis_from_net(task_params: ClusteringTaskParameters,
                                 ssd_config: dict, image_size: ImageSizeTuple, logger=None):

    ssd_net = generate_net_with_priors(image_size, ssd_config, task_params, logger)

    fm = [np.array(info['feature_sizes_wh']) for _, info in ssd_net.priors_info.items()]
    feature_maps = np.vstack(fm)
    prs = [np.array(prior) for _, info in ssd_net.priors_info.items() for prior in info['basis']]
    priors_wh = np.vstack(prs)
    mgroup = [mg for mg, prior_info in ssd_net.priors_info.items() for _ in prior_info['basis']]
    prior_match_group = np.hstack(mgroup)

    return np.round(priors_wh, decimals=3), prior_match_group, np.array(image_size) / feature_maps


def create_prior_df(priors, match_group):
    priors_df = pandas.DataFrame(priors, columns=['width', 'height'])
    priors_df['match_group'] = match_group
    # add more data
    priors_df['area'] = area_from_df(priors_df)
    priors_df['aspect_ratio'] = aspect_ratio_from_df(priors_df)
    priors_df = priors_df[['match_group', 'area', 'aspect_ratio', 'width', 'height']]
    priors_df = priors_df.sort_values(['match_group', 'area', 'aspect_ratio']).reset_index(drop=True)
    priors_df['index'] = list('prior_{}'.format(i) for i in range(len(priors)))
    priors_df = priors_df.set_index('index')
    return priors_df


def xywh_to_ttbb(xywh_box: np.ndarray):
    whalf = np.floor_divide(xywh_box[:, 2], 2)
    hhalf = np.floor_divide(xywh_box[:, 3], 2)
    return np.array([
        # tx =
        xywh_box[:, 0] - whalf,
        # ty =
        xywh_box[:, 1] - hhalf,
        # bx =
        xywh_box[:, 0] + whalf,
        # by =
        xywh_box[:, 1] + hhalf]).T.astype(np.int)


def ttbb_box_area(ttbb_box: np.ndarray):
    if len(ttbb_box.shape) < 2:
        raise ValueError('expected atleast 2d array here')
    return (ttbb_box[:, 2] - ttbb_box[:, 0]) * (ttbb_box[:, 3] - ttbb_box[:, 1])


def compute_intersection_with_single_box(single_box: np.ndarray, boxes: np.ndarray):
    txy = np.maximum(single_box[:, 0:2], boxes[:, 0:2])
    bxy = np.minimum(single_box[:, 2:4], boxes[:, 2:4])
    return np.hstack((txy, bxy))


def fast_iou_one_many(ttbb_box, ttbb_boxes):
    one_box_area = ttbb_box_area(ttbb_box)
    many_boxes_area = ttbb_box_area(ttbb_boxes)
    overlap_boxes = compute_intersection_with_single_box(ttbb_box, ttbb_boxes)
    overlap_area = ttbb_box_area(overlap_boxes)
    # broadcasting ;)
    return overlap_area / ((one_box_area + many_boxes_area).astype(np.float32) - overlap_area)


def one_to_many_iou(box: np.ndarray, many_box: np.ndarray, box_aligner=None):
    shifts = box_aligner(box, many_box)
    ttbb_box = xywh_to_ttbb(np.atleast_2d(np.hstack((np.zeros(2), box))))
    ttbb_other_boxes = xywh_to_ttbb(np.hstack((shifts, many_box)))
    return fast_iou_one_many(ttbb_box, ttbb_other_boxes)


def one_minus_one_to_many_iou(box_many_box_tuple, box_aligner=None):
    box, many_box = box_many_box_tuple
    box_aligner = box_aligner if box_aligner else BoxAlignCalculator(align_strategy=AlignTypeEnum.BottomLeft)
    return 1 - one_to_many_iou(box=box, many_box=many_box, box_aligner=box_aligner)


def pairwise_iou(box_wh_list: np.ndarray, box_aligner=None, full=False):
    assert box_wh_list.shape[1] == 2, 'expecting an array of w,h'
    if box_aligner is None:
        box_aligner = BoxAlignCalculator(align_strategy=AlignTypeEnum.BottomLeft)
    assert callable(box_aligner), 'wrong box aligner type'

    box_wh_list = np.around(box_wh_list).astype(dtype=np.int)

    ious = []
    for i, box in enumerate(box_wh_list):
        shifts = box_aligner(box, box_wh_list[i + 1:])
        ttbb_box = xywh_to_ttbb(np.atleast_2d(np.hstack((np.zeros(2), box))))
        ttbb_other_boxes = xywh_to_ttbb(np.hstack((shifts, box_wh_list[i + 1:])))
        ious.append(fast_iou_one_many(ttbb_box, ttbb_other_boxes))

    data = np.concatenate(ious)

    n = len(box_wh_list)
    if full:
        result = np.identity(n, dtype=np.float32)
        indices = np.triu_indices(n, 1)
        result[indices] = data
        # but also
        result.T[indices] = data
    else:
        result = np.zeros((n * (n - 1) // 2))
        result[:] = data[:]

    return result


def index_to_prior_name_map(column_name_pattern: str, prior_table: pandas.DataFrame) -> OrderedDict:
    mapping = OrderedDict()
    for prior_idx in prior_table.index:
        column_name = column_name_pattern.format(prior_idx)
        mapping[prior_idx] = column_name
    return mapping


def get_pipe_cls_and_frame_cls_kwargs(frame_cls_kwargs):
    if frame_cls_kwargs is None:
        frame_cls_kwargs = dict()
    if 'crop_width' in frame_cls_kwargs.keys():
        default_overlap_ratio = 0.1
        frame_cls_kwargs_for_crop = frame_cls_kwargs.copy
        frame_cls_kwargs_for_crop.setdefault('overlap_percent', default_overlap_ratio)
        return ScanningDataPipe, frame_cls_kwargs_for_crop
    else:
        return DataPipe, frame_cls_kwargs


def get_box_pairwise_iou(boxes):
    # TODO - rewrite to be more maintainable
    def _get_boxes_area(box_array):
        return np.prod(np.maximum(np.diff(box_array[:, [0, 2, 1, 3]], axis=1)[:, [0, 2]], 0), axis=1)

    n_boxes = len(boxes)
    left_ = boxes.repeat(n_boxes, 0)  # repeat boxes on x-array
    right_ = boxes.reshape(-1, 1).repeat(n_boxes, 1).T.reshape(-1, 4)  # repeat boxes on y-array
    intersection_boxes = np.zeros_like(left_)
    for k in [2, 3]:
        intersection_boxes[:, k] = np.hstack((left_[:, k:k+1], right_[:, k:k+1])).min(1)
    for k in [0, 1]:
        intersection_boxes[:, k] = np.hstack((left_[:, k:k + 1], right_[:, k:k + 1])).max(1)
    inter_area = _get_boxes_area(intersection_boxes).reshape(n_boxes, n_boxes)
    union_area = (_get_boxes_area(left_) + _get_boxes_area(right_)).reshape(n_boxes, n_boxes) - inter_area
    iou = inter_area / union_area
    return iou


def get_all_bboxes_in_n_frames(dataview: DataView,
                               frame_kwargs: dict,
                               n_samples: int,
                               batch_size: int = 128) -> pandas.DataFrame:

    def get_item_for_stats(image_frame: ImageFrame):
        orig_boxes = [box['box'] for box in image_frame.get_boxes()]
        if len(orig_boxes) == 0:
            image_frame.release_data()
            return []
        orig_labels = [box['labels']['id'] for box in image_frame.get_boxes()]

        orig_boxes = np.around(orig_boxes)
        width = np.around(orig_boxes[:, 2]-orig_boxes[:, 0])
        height = np.around(orig_boxes[:, 3]-orig_boxes[:, 1])
        pairwise_iou = get_box_pairwise_iou(orig_boxes)
        score = np.around(pairwise_iou.sum(axis=0) - 1, decimals=2)

        image_frame.release_data()
        return [(w, h, label, q) for w, h, label, q in zip(width, height, orig_labels, score)]

    def collate_item_for_stats(items):
        return [item for frame_list in items for item in frame_list]

    big_batch_size = batch_size
    pipe_cls, frame_kwargs = get_pipe_cls_and_frame_cls_kwargs(frame_kwargs)
    stat_pipe = pipe_cls(iterator_factory=dataview, batch_size=big_batch_size,
                         get_item_fn=get_item_for_stats,
                         collate_fn=collate_item_for_stats,
                         frame_cls_kwargs=frame_kwargs)

    iterator = tqdm(stat_pipe.get_iterator(), total=n_samples//big_batch_size,
                    desc='Collecting data batches...')
    stats = []
    for items in iterator:
        stats.extend(items)

    column_names=['width', 'height', 'label', 'overlap_score']
    return pandas.DataFrame(stats, columns=column_names)


def aspect_ratio_from_df(df: pandas.DataFrame, columns=('width', 'height')):
    assert len(columns) == 2
    return np.divide(df[columns[0]].values, df[columns[-1]].values)


def humanized_aspect_ratio_from_df(df: pandas.DataFrame, columns=('width', 'height')):
    assert len(columns) == 2
    ar = np.divide(df[columns[0]].values, df[columns[-1]].values)
    ar_m1 = ar - 1
    ra = np.divide(df[columns[-1]].values, df[columns[0]].values)
    m_ra_m1 = -(ra - 1)
    return np.where(ar >= 1, ar_m1, m_ra_m1)


def area_from_df(df: pandas.DataFrame, columns=('width', 'height')):
    return np.prod(df[list(columns)].values, axis=1)


def bbox_log_distance(boxes, axis=0):
    return np.exp(np.median(np.log(boxes), axis=axis))


def bbox_wh_relative_metric(box_many_box_tuple):
    box, many_box = box_many_box_tuple
    return np.sqrt(np.sum(np.power(np.log(many_box / box), 2), axis=1))


def target_iou(target_box_wh: np.ndarray, incoming_box_wh: np.ndarray, center_deltas_xy: np.ndarray,
               visualize: bool = False, vis_options: dict = None) -> np.ndarray:
    # todo raise exceptions instead
    # assert target_box_wh.size == 2, "single target box supported"
    # assert incoming_box_wh.size == 2, "single incoming box"
    # assert center_deltas_xy.shape[-1] == 2

    center_deltas_xy = np.atleast_2d(center_deltas_xy)

    incoming_box_wh_replica = np.tile(incoming_box_wh, len(center_deltas_xy)).reshape(-1, 2)
    incoming_boxes = np.hstack((center_deltas_xy, incoming_box_wh_replica))
    boxes_with_shift_ttbb = xywh_to_ttbb(incoming_boxes)

    target_box_ttbb = xywh_to_ttbb(np.array([[0, 0, *target_box_wh]]))
    overlap_boxes = compute_intersection_with_single_box(target_box_ttbb, boxes_with_shift_ttbb).astype(np.float32)

    target_box_area = target_box_wh[0] * target_box_wh[1]
    incoming_box_area = incoming_box_wh[0] * incoming_box_wh[1]
    overlap_area = ttbb_box_area(overlap_boxes).astype(np.float32)
    # some broadcasting...
    iou = overlap_area / ((target_box_area + incoming_box_area).astype(np.float32) - overlap_area)

    if visualize:
        viz_boxes_with_shift(center_deltas_xy, incoming_box_wh, iou, target_box_wh, vis_options)

    return iou


def viz_boxes_with_shift(deltas_xy, in_box_wh, precomp_iou, target_box_wh, vis_options):
    vis_options = create_default_vis_options() if vis_options is None else vis_options
    if np.any(precomp_iou > vis_options['min_iou_to_viz']):
        patchkes = []
        w, h = target_box_wh
        w_in, h_in = in_box_wh
        disp_w = min(w, vis_options['display_size'].w // 2)
        disp_h = min(h, vis_options['display_size'].h // 2)
        # matplotlib:
        ax = plt.gca()
        ax.cla()
        ax.set_xlim((-1 * disp_w, disp_w))
        ax.set_ylim((-1 * disp_h, disp_h))
        bottom_left = (-1 * np.floor_divide(w, 2), -1 * np.floor_divide(h, 2))
        top_left = (-1 * np.floor_divide(w, 2), np.floor_divide(h, 2))
        bottom_left_in_unshifted = [-1 * np.floor_divide(w_in, 2), -1 * np.floor_divide(h_in, 2)]
        target_rect = patches.Rectangle(bottom_left, width=w, height=h,
                                        alpha=0.5, linewidth=2, edgecolor='b', facecolor='b')
        ax.text(top_left[0], top_left[1], '{} x {}'.format(w, h), fontsize=12)
        patchkes.append(target_rect)
        for i, (an_iou, delta) in enumerate(zip(precomp_iou, deltas_xy)):
            color = 'C' + str(i % 5 + 3)
            bottom_left_in = (bottom_left_in_unshifted[0] + delta[0], bottom_left_in_unshifted[1] + delta[1])
            if an_iou > vis_options['min_iou_to_viz']:
                rect = patches.Rectangle(bottom_left_in,
                                         width=w_in, height=h_in, linewidth=1, facecolor=color, alpha=0.8)
                ax.text(bottom_left_in[0], bottom_left_in[1],
                        '{} x {} @ {:.3f}'.format(w_in, h_in, an_iou), fontsize=12)
            else:
                rect = patches.Rectangle(bottom_left_in,
                                         width=w_in, height=h_in, linewidth=1, facecolor=color, alpha=0.1)

            patchkes.append(rect)

        rect_collection = PatchCollection(patchkes, match_original=True)
        ax.add_collection(collection=rect_collection)
        plt.pause(w_in * h_in / float(w * h) * vis_options['base_pause_length'])


