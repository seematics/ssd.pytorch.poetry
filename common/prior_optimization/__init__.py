import enum

import numpy as np
from allegroai.task_parameters import TaskParameters, param

from common.utils import ImageSizeTuple


class PriorOptimizationOptions(TaskParameters):
    n_samples = param(type=int, range=[10, None], desc='Number of samples to draw upon from dataview for analysis')
    max_n_clusters = param(type=int, range=[1, 15], desc='Max number of clusters to create from drawn samples')
    cluster_threshold = param(type=float, range=[0, 1], desc='Used to threshold cluster importance')
    target_size_w = param(type=int, range=[100, None], desc='Limit the width of images to max this value in pixels')
    target_size_h = param(type=int, range=[100, None], desc='Limit the height of images to max this value in pixels')
    equalize_dataview_queries = param(type=bool, desc='Add equal weights to all labels in dataview (experimental)')
    resize_strategy = param(type=str, desc='how to resize images in case not using scannign mode')
    scanning_mode = param(type=bool, desc='use scanning mode pipe (advanced)')


class AlignTypeEnum(enum.Enum):
    BottomLeft = 'bottom_left'
    Nop = 'no_op'
    Random = 'random'
    MultiRandom = 'multi_random'


class AlignTypeFunctions:
    N_MULTI_RANDOM = 8

    @staticmethod
    def nop(box1: np.ndarray, box2: np.ndarray) -> np.ndarray:
        # return np.tile(np.zeros_like(box1), 10).reshape(-1, 2)
        return np.zeros_like(box1)

    @staticmethod
    def bottom_left(box1: np.ndarray, box2: np.ndarray) -> np.ndarray:
        return np.atleast_2d(0.5 * (box2 - box1))

    @staticmethod
    def random(box1: np.ndarray, box2: np.ndarray) -> np.ndarray:
        max_shift_w = np.floor_divide(box1[0] + box2[0], 2)
        max_shift_h = np.floor_divide(box1[1] + box2[1], 2)
        return np.array([[np.random.randint(low=-max_shift_w, high=max_shift_w),
                          np.random.randint(low=-max_shift_h, high=max_shift_h)]])

    @staticmethod
    def multirandom(box1: np.ndarray, box2: np.ndarray) -> np.ndarray:
        if box1[0] > box2[0] * 2 and box1[1] > box2[1] * 2:
            return np.array([0, 0])
        max_shift_w = np.floor_divide(box1[0] + box2[0], 2)
        max_shift_h = np.floor_divide(box1[1] + box2[1], 2)
        return np.hstack((np.random.randint(low=-max_shift_w,
                                            high=max_shift_w,
                                            size=(AlignTypeFunctions.N_MULTI_RANDOM, 1)),
                          np.random.randint(low=-max_shift_h,
                                            high=max_shift_h,
                                            size=(AlignTypeFunctions.N_MULTI_RANDOM, 1))))


class BoxAlignCalculator(object):
    def __init__(self, align_strategy: AlignTypeEnum = AlignTypeEnum.Nop, vectorize: bool = False):
        self._align_func = self._get_align_function(align_strategy, vectorize)
        if not self._align_func:
            raise TypeError('align_strategy must be of type AlignTypeEnum')

    @staticmethod
    def __get_align_func(align_type):
        return {
            AlignTypeEnum.BottomLeft: AlignTypeFunctions.bottom_left,
            AlignTypeEnum.Nop: AlignTypeFunctions.nop,
            AlignTypeEnum.Random: AlignTypeFunctions.random,
            AlignTypeEnum.MultiRandom: AlignTypeFunctions.multirandom,
        }.get(align_type)

    @staticmethod
    def _get_align_function(align_type, vectorize=False):
        align_func = BoxAlignCalculator.__get_align_func(align_type)
        if align_func is None:
            return None
        if vectorize:
            Warning('Caveat Emptor: Support for vectorized align types is minimal and most of this is untested')
            return np.vectorize(align_func)
        else:
            return align_func

    def __call__(self, box1: np.ndarray, box2: np.ndarray):
        return self._align_func(box1, box2)


KEEP_DATA = True


def create_default_vis_options():
    return dict(viz_flag=False, display_size=ImageSizeTuple(640, 640), base_pause_length=0.04, min_iou_to_viz=0.5)