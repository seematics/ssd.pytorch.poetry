import numpy as np

# Reset timers every N reports (if timing)
TIMERS_BENCHMARK_WINDOW = 10

# averaging loss over the last N records
LOSS_AVERAGING_WINDOW = 100
EPS = np.finfo(float).eps
DEFAULT_SLIDING_WINDOW_OVERLAP_RATIO = 0.2


def get_width_height_from_frame_source(frame, source_id):
    if source_id is None:
        return frame.sources[0].width, frame.sources[0].height
    wh_tup = [(source.width, source.height) for source in frame.sources if source.id == source_id]
    if len(wh_tup) == 0:
        raise ValueError("source_id: {} does not exist in frame".format(source_id))
    return wh_tup[0]


def get_uri_from_frame_source(frame, source_id):
    if source_id is None:
        return frame.sources[0].uri
    uris = [source.uri for source in frame.sources if source.id == source_id]
    if len(uris) == 0:
        raise ValueError("source_id: {} does not exist in frame".format(source_id))
    return uris[0]
