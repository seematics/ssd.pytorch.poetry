from collections import namedtuple

import numpy as np
from functools import partial

# from numba import jit, float64
from allegroai.utilities.numerical import nms_by_score

MatchingResult = namedtuple('MatchingResult', ['predicted', 'actual', 'correct'])


class RunningFScore(object):
    EPS = 0.00001

    def __init__(self, iou_thresh, num_of_labels, batch_size):
        self.batch_size = batch_size
        self.iou_thresh = iou_thresh

        # Rows are labels, columns are total predicted, actual instances, correctly predicted
        self._num_labels = num_of_labels
        self.total_results = np.zeros(shape=(self._num_labels, 3))

    def reset(self):
        self.total_results = np.zeros(shape=(self._num_labels, 3))

    @property
    def active_classes(self):
        return np.where(self.total_results[:, 1] > 0)[0]

    def f_score(self, beta=1):
        recall_cls, recall = self.recall
        prec_cls, prec = self.precision
        f_score_cls = _f_score(beta=beta, recall=recall_cls, prec=prec_cls)
        f_score_total = _f_score(beta=beta, recall=recall, prec=prec)

        return f_score_cls, f_score_total

    @property
    def recall(self):
        per_class_recall = self.total_results[:, 2] / (self.total_results[:, 1] + RunningFScore.EPS)
        total_stats = self.total_results[self.active_classes, :].sum(0)
        total_recall = total_stats[2] / (total_stats[1] + RunningFScore.EPS)
        return per_class_recall, total_recall

    @property
    def precision(self):
        per_class_prec = self.total_results[:, 2] / (self.total_results[:, 0] + RunningFScore.EPS)
        total_stats = self.total_results[self.active_classes, :].sum(0)
        total_prec = total_stats[2] / (total_stats[0] + RunningFScore.EPS)
        return per_class_prec, total_prec

    def update_metrics(self, pred_boxes, gt_boxes):
        """
        :param gt_boxes: np.array of shape (num boxes, 6) where the 5 is [batch idx, x1, y1, x2, y2, label]
        :param pred_boxes: np.array of shape (num boxes, 6) where the 5 is [batch idx, x1, y1, x2, y2, label]
        """
        for pred_boxes_labels, gt_boxes_labels in self._frame_iterator(pred_boxes, gt_boxes, self.batch_size):
            self._update_metrics_table_from_frame(pred_boxes_labels, gt_boxes_labels)

    @staticmethod
    def _frame_iterator(pred_boxes, gt_boxes, batch_size):
        """
        :param gt_boxes: np.array of shape (num boxes, 6) where the 5 is [batch idx, x1, y1, x2, y2, label]
        :param pred_boxes:  np.array of shape (num boxes, 6) where the 5 is [batch idx, x1, y1, x2, y2, label]
        :type batch_size: int number of frames per batch
        :return:
        """
        pred_boxes_frame = np.atleast_2d(np.array([]))
        gt_boxes_frame = np.atleast_2d(np.array([]))

        for idx in range(batch_size):
            if pred_boxes.size:
                pred_idx = np.where(pred_boxes[:, 0] == idx)[0]
                pred_boxes_frame = pred_boxes[pred_idx, 1:]
            if gt_boxes.size:
                gt_idx = np.where(gt_boxes[:, 0] == idx)[0]
                gt_boxes_frame = gt_boxes[gt_idx, 1:]

        yield pred_boxes_frame, gt_boxes_frame

    def _update_metrics_table_from_frame(self, pred_boxes_labels, gt_boxes_labels):
        results = get_predicted_actual_correct_count_from_frame(pred_boxes_labels, gt_boxes_labels,
                                                                iou_thresh=self.iou_thresh)
        for label, res in results.items():
            self.total_results[label, 0] += res.predicted
            self.total_results[label, 1] += res.actual
            self.total_results[label, 2] += res.correct


class BoxesHistory(object):

    def __init__(self):
        # history for Prec-Recall curve
        self._all_pred_boxes_with_labels_scores_and_frame_id = []
        self._all_gt_boxes_with_labels_and_frame_id = []

        self._frame_index = 0
        self._pred_box_index = 0
        self._gt_box_index = 0

    @property
    def pred_boxes(self):
        return self._all_pred_boxes_with_labels_scores_and_frame_id

    @property
    def gt_boxes(self):
        return self._all_gt_boxes_with_labels_and_frame_id

    def save_frame_data(self, pred_boxes, pred_scores, gt_boxes):
        """
        Saves predicted boxes and score and ground truth boxes for calculation of Precision-Recall curve

        :param pred_boxes: np.array of shape (num boxes, 6) where the 5 is [batch idx, x1, y1, x2, y2, label]
        :param pred_scores: np.array of shape (num_boxes, ) where each entry is an np.float32 prediction score
        :param gt_boxes: np.array of shape (num boxes, 6) where the 5 is [batch idx, x1, y1, x2, y2, label]
        :return: (None),
                saves each pred box in [frame_id, pred_box_id, x1, y1, x2, y2, label, score]
                saves each gt box in [frame_id, gt_box_id, x1, y1, x2, y2, label]
        """
        unique_bidx = set()

        if pred_boxes is not None and pred_boxes.size:
            for bidx in np.unique(pred_boxes[:, 0]):
                unique_bidx.add(bidx)

            pred_boxes = np.atleast_2d(pred_boxes)

        if gt_boxes is not None and gt_boxes.size:
            for bidx in np.unique(gt_boxes[:, 0]):
                unique_bidx.add(bidx)

            gt_boxes = np.atleast_2d(gt_boxes)

        for bidx in unique_bidx:
            if pred_boxes is not None and pred_boxes.size:
                pred_bidx = np.where(pred_boxes[:, 0] == bidx)[0]
                for pidx in pred_bidx:
                    new_box = [self._frame_index, self._pred_box_index] + \
                              list(pred_boxes[pidx, 1:]) + [round(pred_scores[pidx], 2)]
                    self._all_pred_boxes_with_labels_scores_and_frame_id.append(new_box)
                    self._pred_box_index += 1

            if gt_boxes is not None and gt_boxes.size:
                gt_idx = np.where(gt_boxes[:, 0] == bidx)[0]
                for gtidx in gt_idx:
                    new_box = [self._frame_index, self._gt_box_index] + list(gt_boxes[gtidx, 1:])
                    self._all_gt_boxes_with_labels_and_frame_id.append(new_box)
                    self._gt_box_index += 1

            self._frame_index += 1

    def get_precision_recall_curve(self, iou_threshold, conf_thresholds=None, nms_threshold=0.65):
        pr_curves = get_raw_precision_recall_curve(self.pred_boxes, self.gt_boxes,
                                                   iou_threshold=iou_threshold,
                                                   thresholds=conf_thresholds,
                                                   nms_threshold=nms_threshold)

        for label in pr_curves:
            before, after = BoxesHistory._prec_recall_padding(pr_curves[label].raw_curve)
            pr_curves[label].pad(before, after)

        return pr_curves

    @staticmethod
    def _prec_recall_padding(raw_pr_curve):
        best_precision = raw_pr_curve[0][1]
        first_point = (1.0, best_precision, 0.0)
        best_recall = raw_pr_curve[-1][2]
        last_point = (0.0, 0.0, best_recall + 0.0001)
        show_recall_one = (0, 0.0, 1.0)
        return [first_point], [last_point, show_recall_one]


def average_precision_from_curve(pr_curve, min_precision=0.0):
    dx = np.diff(pr_curve.recall)
    adj_precision = np.maximum(pr_curve.precision - min_precision, 0.0)
    lower_bound = np.sum(dx * adj_precision[1:])
    upper_bound = np.sum(dx * adj_precision[:-1])
    un_normalized = np.mean([lower_bound, upper_bound])
    normalization = np.maximum(1.0 * (1.0 - min_precision), 0.00000001)
    return un_normalized / normalization


class PrecisionRecallCurve(object):

    def __init__(self):
        self._curve = []

    def add(self, conf, precision, recall):
        self._curve.append((conf, precision, recall))

    @property
    def raw_curve(self):
        return self._curve

    @property
    def conf(self):
        return np.array([p[0] for p in self._curve])

    @property
    def precision(self):
        return np.array([p[1] for p in self._curve])

    @property
    def recall(self):
        return np.array([p[2] for p in self._curve])

    def pad(self, before=None, after=None):

        def _verify(items):
            if not isinstance(items, list):
                raise ValueError('Padding items must be lists')

            ver_items = [all((isinstance(item, (list, tuple)), len(item) == 3)) for item in items]

            for idx, v in enumerate(ver_items):
                if not v:
                    msg = 'Item must be tuple of (conf, precision, recall) got %s in position %d'
                    raise ValueError(msg % (str(items[idx]), idx))

        if before is not None:
            _verify(before)
            self._curve = before + self._curve

        if after is not None:
            _verify(after)
            self._curve = self._curve + after


def get_raw_precision_recall_curve(all_pred_boxes, all_gt_boxes, iou_threshold, thresholds=None,
                                   nms_threshold=-1.0, verbose=False):
    """
    :param all_gt_boxes:  [frame_id, gt_box_id, x1, y1, x2, y2, label]
    :param all_pred_boxes: [frame_id, pred_box_id, x1, y1, x2, y2, label, score]
    :param thresholds: list of floats, what thresholds to include in a curve  e.g. [0.2, 0.4, 0.6, 0.8]
    :param iou_threshold: IoU at which a box will be considered a match
    :param nms_threshold: IoU for nms performed to predicted boxes before matching (preprocessing)
    :param verbose: for debug - also print predictions (not to log)
    :return: list of points on a precision-recall curve for all classes (class_id, min_conf_thresh, precision, recall)
    """
    all_pred_boxes = np.array(all_pred_boxes)
    all_gt_boxes = np.array(all_gt_boxes)

    # Scores are rounded to 2 dec. so at most we'll gave 10K of those
    if thresholds is None and all_pred_boxes.size:
        thresholds = np.unique(np.minimum(0.99, np.maximum(all_pred_boxes[:, 7], 0.01)))

    desc_thresholds = sorted(thresholds)[::-1]

    points_on_curve = []
    raw_curve_per_label = {}

    last_frame_id = max(int(all_gt_boxes[:, 0].max()) + 1 if all_pred_boxes.size else 0,
                        int(all_pred_boxes[:, 0].max()) + 1 if all_pred_boxes.size else 0)

    for min_score in desc_thresholds:
        cummul_matching_results = {}
        curr_pred_boxes_idx = (all_pred_boxes[:, 7] >= min_score) if all_pred_boxes.size else np.array([])
        curr_pred_boxes = all_pred_boxes[curr_pred_boxes_idx] if curr_pred_boxes_idx.size else np.array([])

        for frame_id in range(last_frame_id):
            frame_pred_boxes_labels_idx = np.where((curr_pred_boxes[:, 0] == frame_id))[0] \
                if curr_pred_boxes.size else np.array([])
            frame_gt_labels_idx = np.where(all_gt_boxes[:, 0] == frame_id)[0]\
                if all_gt_boxes.size else np.array([])

            # Take coordinates and label only
            pred_boxes = curr_pred_boxes[frame_pred_boxes_labels_idx, 2:-1]\
                if frame_pred_boxes_labels_idx.size else np.array([])
            # nms
            if nms_threshold > 0:
                pred_scores = curr_pred_boxes[frame_pred_boxes_labels_idx, 7] \
                    if frame_pred_boxes_labels_idx.size else np.array([])
                keep = nms_by_score(pred_boxes, pred_scores, overlapThresh=nms_threshold)
                pred_boxes = pred_boxes[keep]

            gt_boxes = all_gt_boxes[frame_gt_labels_idx, 2:] if frame_gt_labels_idx.size else np.array([])
            # TODO: Refactor to optimize - no need to calculate overlaps for all conf. thresholds
            res_by_label = get_predicted_actual_correct_count_from_frame(pred_boxes, gt_boxes, iou_thresh=iou_threshold)
            for label in res_by_label:
                count = cummul_matching_results.get(label, MatchingResult(0, 0, 0))
                res = res_by_label[label]
                cummul_matching_results[label] =\
                    MatchingResult(count.predicted + res.predicted,
                                   count.actual + res.actual,
                                   count.correct + res.correct)

                if raw_curve_per_label.get(label) is None:
                    raw_curve_per_label[label] = PrecisionRecallCurve()

        for label, res in cummul_matching_results.items():
            if verbose:
                print('label', label, 'Num GTs', res.actual, 'Num PREDs', res.predicted, 'score', min_score)
            precision = float(res.correct) / (res.predicted + RunningFScore.EPS)
            recall = float(res.correct) / (res.actual + RunningFScore.EPS)
            raw_curve_per_label[label].add(min_score, precision, recall)

            point = (label, min_score, precision, recall)
            points_on_curve.append(point)

    return raw_curve_per_label


def get_predicted_actual_correct_count_from_frame(pred_boxes_labels, gt_boxes_labels, iou_thresh):
    if pred_boxes_labels.size and gt_boxes_labels.size:
        # Make sure we ignore all detection on hard rois
        overlaps_matrix = bbox_overlaps(row_boxes=pred_boxes_labels[:, :-1], col_boxes=gt_boxes_labels[:, :-1])
        best_fitting_idx = overlaps_matrix.argmax(1)
        best_fitting_vals = overlaps_matrix.max(1)
        best_fitting_labels = gt_boxes_labels[best_fitting_idx, -1]

        valid_pred_idx, = np.where(
            (best_fitting_labels >= 0) | ((best_fitting_labels < 0) & (best_fitting_vals < iou_thresh)))
        pred_boxes_labels = pred_boxes_labels[valid_pred_idx, :]

        valid_gt_idx, = np.where(gt_boxes_labels[:, -1] >= 0)
        gt_boxes_labels = gt_boxes_labels[valid_gt_idx, :]

        # Clear overlap matrix of invalid boxes
        all_overlaps_matrix = overlaps_matrix[valid_pred_idx, :][:, valid_gt_idx]
    # else:
    #     all_overlaps_matrix = np.array([])

    results = {}

    unique_labels_in_frame = set()
    if pred_boxes_labels.size:
        for label in pred_boxes_labels[:, -1]:
            if label > 0:
                unique_labels_in_frame.add(label)

    if gt_boxes_labels.size:
        for label in gt_boxes_labels[:, -1]:
            if label > 0:
                unique_labels_in_frame.add(label)

    for lab_id in unique_labels_in_frame:

        label = int(lab_id)

        pred_idx = np.where(pred_boxes_labels[:, -1] == label)[0] if pred_boxes_labels.size else np.array([])
        predicted = pred_idx.shape[0]

        gt_idx = np.where(gt_boxes_labels[:, -1] == label)[0] if gt_boxes_labels.size else np.array([])
        actual = gt_idx.shape[0]

        if not gt_idx.size or not pred_idx.size:
            correct = 0
        else:
            overlaps_matrix = all_overlaps_matrix[pred_idx, :][:, gt_idx]
            pred_gt_cover, gt_pred_cover = get_coverage_mappings(overlaps_matrix,
                                                                 iou_thresh=iou_thresh, allow_multiple=False)
            correct = len(gt_pred_cover)

        results[label] = MatchingResult(predicted, actual, correct)

    return results


def _f_score(beta, recall, prec):
    b_sq = beta ** 2
    numerator = recall * prec
    denomenator = b_sq * prec + recall
    res = (1 + b_sq) * (numerator / (denomenator + RunningFScore.EPS))
    return res


def get_coverage_mappings(overlaps, iou_thresh, allow_multiple=False):
    """
    Find best coverage of gt_boxes by predicted boxes
    :param overlaps: assumes (boxes, gt_boxes)
    :param iou_thresh: min. IoU threshold to be considered a match
    :param allow_multiple: Allow usage of a box more than once (several boxes may cover other boxes)
    :return:
        col_box -> row_box coverage map
        row_box -> col_box coverage map
    """
    rows = overlaps.shape[0]
    cols = overlaps.shape[1]

    convert_1d_2d = partial(index_1to2d, fast_shift=cols)

    flat_overlaps = overlaps.flatten()
    indx_best_fits = np.argsort(flat_overlaps)[::-1]

    col_to_row = {}
    row_to_col = {}
    row_bucket = np.zeros(rows)
    col_bucket = np.zeros(cols)

    for ind in indx_best_fits:
        row, col = convert_1d_2d(ind)
        iou = flat_overlaps[ind]
        if iou >= iou_thresh:
            if not row_bucket[row] and not col_bucket[col]:
                row_bucket[row] = 1
                row_to_col[row] = col
                col_bucket[col] = 1
                col_to_row[col] = row

            elif not row_bucket[row] and allow_multiple:
                row_bucket[row] = 1
                row_to_col[row] = col

            elif not col_bucket[col] and allow_multiple:
                col_bucket[col] = 1
                col_to_row[col] = row

    return row_to_col, col_to_row


def index_1to2d(idx, fast_shift=1):
    slow = np.floor(idx / fast_shift).astype(np.int32)
    fast = idx % fast_shift
    return slow, fast


def py_bbox_overlaps(row_boxes, col_boxes):
    """
    Parameters
    ----------
    row_boxes: (N, 4) ndarray of float
    col_boxes: (K, 4) ndarray of float
    Returns
    -------
    overlaps: (N, K) ndarray of overlap between row_boxes and col_boxes
    """
    N = row_boxes.shape[0]
    K = col_boxes.shape[0]
    overlaps = np.zeros((N, K), dtype=np.float32)
    for k in range(K):
        box_area = (
                (col_boxes[k, 2] - col_boxes[k, 0] + 1) *
                (col_boxes[k, 3] - col_boxes[k, 1] + 1)
        )
        for n in range(N):
            iw = (
                    min(row_boxes[n, 2], col_boxes[k, 2]) -
                    max(row_boxes[n, 0], col_boxes[k, 0]) + 1
            )
            if iw > 0:
                ih = (
                        min(row_boxes[n, 3], col_boxes[k, 3]) -
                        max(row_boxes[n, 1], col_boxes[k, 1]) + 1
                )
                if ih > 0:
                    ua = float(
                        (row_boxes[n, 2] - row_boxes[n, 0] + 1) *
                        (row_boxes[n, 3] - row_boxes[n, 1] + 1) +
                        box_area - iw * ih
                    )
                    overlaps[n, k] = iw * ih / ua
    return overlaps


bbox_overlaps = py_bbox_overlaps
# bbox_overlaps = jit(float64[:, :](float64[:, :], float64[:, :]))(py_bbox_overlaps)
