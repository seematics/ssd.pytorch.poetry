from __future__ import division
from math import sqrt as sqrt
from collections import namedtuple, OrderedDict
import torch

from abc import ABCMeta, abstractmethod


NEW_ASPECT_RATIO_KEY = 'full_aspect_ratios'

AnchorProto = namedtuple('AnchorProto', 'w h')
OFFSET_X = 0.5
OFFSET_Y = 0.5

V1KEYS = ('min_sizes', 'max_sizes', 'aspect_ratios', NEW_ASPECT_RATIO_KEY)


class PriorBoxBase(metaclass=ABCMeta):

    def __init__(self, cfg, override_feature_sizes_wh=None, logger=None):
        super(PriorBoxBase, self).__init__()
        self._logger = logger
        self._incoming_feature_sizes = override_feature_sizes_wh
        is_v1 = PriorBoxBase.detect_prior_v1(cfg=cfg)
        if is_v1:
            if self._logger:
                self._logger.warn('Now converting ssd config to new format. make sure to save the updated config')
            cfg = PriorBoxBase.__convert_cfg_from_prior_v1(cfg, self._incoming_feature_sizes)

        self.image_size = AnchorProto(w=cfg['input_dim_w'], h=cfg['input_dim_h'])
        self.prior_design = PriorBoxBase._prior_design_from_dict(cfg['prior_design'], self.image_size)

        self.variance = cfg.get('variance', 0.1)
        self.clip = cfg.get('clip', False)
        for v in self.variance:
            if v <= 0:
                raise ValueError('Variances must be greater than 0')

        if self._incoming_feature_sizes:
            self.feature_maps_wh = self._incoming_feature_sizes
        else:
            try:
                self.feature_maps_wh = [tuple(fmap) for fmap in cfg['feature_maps']]
            except TypeError:
                self.feature_maps_wh = [(fmap, fmap) for fmap in cfg['feature_maps']]
            except KeyError:
                raise KeyError("either input override_feature_maps or 'feature_maps' must be present in cfg"
                               " when the priorbox object is initialized")
            assert len(self.feature_maps_wh) == len(self.prior_design.keys()), \
                "Design error detected: " \
                "Read {scale} aspect ratio scales, But there are {n_feature}" \
                " feature scales".format(scale=len(self.prior_design.keys()), n_feature=len(self.feature_maps_wh))
        cfg['feature_maps'] = [list(fmap) for fmap in self.feature_maps_wh]

    @staticmethod
    def __convert_cfg_from_prior_v1(cfg, feature_maps_wh_override=None):
        try:
            w = cfg['input_dim_w']
            h = cfg['input_dim_h']
        except KeyError:
            try:
                w = cfg['min_dim_w']
                h = cfg['min_dim_h']
                cfg.pop('min_dim_w')
                cfg.pop('min_dim_h')
            except KeyError:
                # deprecation protection
                w = h = cfg['min_dim']
                cfg.pop('min_dim')
            cfg['input_dim_w'] = w
            cfg['input_dim_h'] = h
        try:
            aspect_ratios = cfg[NEW_ASPECT_RATIO_KEY]
        except KeyError:
            # deprecation protection
            cfg[NEW_ASPECT_RATIO_KEY] = PriorBoxBase.__convert_v0_aspect_ratios(cfg['aspect_ratios'])
            cfg.pop('aspect_ratios')
        if feature_maps_wh_override:
            cfg['feature_maps'] = [tuple(reversed(f)) for f in feature_maps_wh_override]
        else:
            if 'feature_maps' not in cfg:
                if cfg['input_dim_h'] == 512:
                    cfg['feature_maps'] = [64, 32, 16, 8, 4, 2, 1]
                else:
                    raise ValueError('Error converting old style priors with no incoming feature_maps sizes')
        prior_design = PriorBoxBase.__create_prior_design_from_v1(cfg, pop_old_values=True)
        image_size = AnchorProto(w=cfg['input_dim_w'], h=cfg['input_dim_h'])
        cfg['prior_design'] = PriorBoxBase._dumpable_prior_design(prior_design, image_size)
        return cfg

    @staticmethod
    def __create_prior_design_from_v1(cfg, pop_old_values=True):
        prior_design = {}
        fmaps = cfg['feature_maps']
        min_sizes = cfg['min_sizes']
        max_sizes = cfg['max_sizes']
        # TODO maybe add assertions to make sure min and max sizes are sorted
        aspect_ratios = cfg[NEW_ASPECT_RATIO_KEY]
        input_size = AnchorProto(w=cfg['input_dim_w'], h=cfg['input_dim_h'])
        fmaps_ordered = sorted(fmaps, reverse=True)
        for k, f in enumerate(fmaps_ordered):
            prior_design[k] = dict()

        for k, f in enumerate(fmaps_ordered):
            if isinstance(f, AnchorProto):
                feature_size_w, feature_size_h = f.w, f.h
            else:
                feature_size_w, feature_size_h = f if isinstance(f, (tuple, list)) else (f, f)
            # get prior basis:
            min_size = min_sizes[k]
            max_size = max_sizes[k]
            s_k = AnchorProto(w=min_size / input_size.w, h=min_size / input_size.h)
            s_k_plus1 = AnchorProto(w=max_size / input_size.w, h=max_size / input_size.h)
            prior_basis = PriorBox.__get_prior_basis_v1(s_k, s_k_plus1, aspect_ratios[k])
            prior_design[k]['feature_sizes_wh'] = (feature_size_w, feature_size_h)
            prior_design[k]['basis'] = prior_basis

        if pop_old_values:
            keys_to_pop = [k for k in V1KEYS if k in cfg]
            for v1_key in keys_to_pop:
                cfg.pop(v1_key)
        return prior_design

    @staticmethod
    def __get_prior_basis_v1(base_prior, large_prior, aspect_ratios):
        prior_basis = []
        s_k = base_prior
        s_k_plus1 = large_prior
        for n, ar in enumerate(aspect_ratios):
            if n == 0 and ar > 0:
                prior_basis.append(AnchorProto(w=s_k.w, h=s_k.h))
            elif n == 1 and ar > 0:
                # aspect_ratio: 1
                # rel size: sqrt(s_k * s_(k+1))
                s_k_prime_w = sqrt(s_k.w * s_k_plus1.w)
                s_k_prime_h = sqrt(s_k.h * s_k_plus1.h)
                prior_basis.append(AnchorProto(w=s_k_prime_w, h=s_k_prime_h))
            elif ar > 0:
                prior_basis.append(AnchorProto(w=s_k.w * sqrt(ar), h=s_k.h / sqrt(ar)))
            elif ar < 0:
                ar = abs(ar)
                prior_basis.append(AnchorProto(w=s_k.w / sqrt(ar), h=s_k.h * sqrt(ar)))
            elif ar == 0:
                continue
            else:
                raise ValueError("aspect ratios not configured correctly")
        return prior_basis

    @staticmethod
    def __convert_v0_aspect_ratios(aspect_ratios):
        new_style_aspect_ratios = []
        for arlist in aspect_ratios:
            new_style = [1, 1]
            for ar in arlist:
                new_style.append(ar)
                new_style.append(-ar)
            new_style_aspect_ratios.append(new_style[:])
        return new_style_aspect_ratios

    @staticmethod
    def detect_prior_v1(cfg):
        return any(key in cfg for key in V1KEYS)

    @staticmethod
    def _dumpable_prior_design(prior_design, image_size):
        design = prior_design.copy()
        for key in sorted(design.keys()):
            design[key]['feature_sizes_wh'] = list(design[key]['feature_sizes_wh'])
            basis = design[key].get('basis') or []
            design[key]['basis'] = [[anchor.w*image_size.w, anchor.h*image_size.h] for anchor in basis]
        return design

    @staticmethod
    def _prior_design_from_dict(prior_design, image_size):
        design = prior_design.copy()
        for key in sorted(design.keys()):
            basis = design[key].get('basis') or []
            basis = [[anchor[0]/image_size.w, anchor[1]/image_size.h] for anchor in basis]
            design[key]['basis'] = [AnchorProto(*anchor) for anchor in basis]
            if 'feature_sizes_wh' in design[key]:
                design[key]['feature_sizes_wh'] = tuple(design[key]['feature_sizes_wh'])
            else:
                design[key]['feature_sizes_wh'] = (-1, -1)
        return design

    @staticmethod
    def get_step_size(im_dim, feature_dim):
        return round(im_dim/feature_dim)

    @abstractmethod
    def forward(self):
        pass

    @classmethod
    def calculate_priors_per_scale(cls, prior_design):
        return [len(prior_design[fmap].get('basis') or []) for fmap in sorted(prior_design.keys())]


class PriorBox(PriorBoxBase):
    """
    Compute priorbox coordinates in center-offset form for each source
    feature map.
    """
    def __init__(self, cfg, override_feature_sizes_wh=None, logger=None):
        super(PriorBox, self).__init__(cfg, override_feature_sizes_wh, logger)

    def forward(self):
        if self._logger:
            self._logger.console('now creating priors')
        boxes = []
        assert all([isinstance(f, AnchorProto) for f in self.feature_maps_wh])
        fmaps_order = sorted(self.feature_maps_wh, reverse=True)
        for k, f in enumerate(fmaps_order):
            this_feature_boxes = []
            feature_size_w, feature_size_h = f.w, f.h
            if self._logger:
                self._logger.console('creating priors for feature map %d with size (w=%d x h=%d)' %
                                     (k, feature_size_w, feature_size_h))
            step_size_w = PriorBox.get_step_size(self.image_size.w, feature_size_w)
            step_size_h = PriorBox.get_step_size(self.image_size.h, feature_size_h)
            if self._logger:
                self._logger.console('auto compute step size ( step_w=%d x step_h=%d )' %
                                     (step_size_w, step_size_h))

            # get prior basis:
            prior_basis = self.prior_design[k].get('basis') or []
            for i in range(feature_size_h):
                for j in range(feature_size_w):
                    # unit center x,y
                    cx = (j + OFFSET_X) * float(step_size_w) / self.image_size.w
                    cy = (i + OFFSET_Y) * float(step_size_h) / self.image_size.h
                    this_point_boxes = [[cx, cy, prior.w, prior.h] for prior in prior_basis]
                    this_feature_boxes.extend(this_point_boxes[:])
            self.prior_design[k]['num_priors'] = len(this_feature_boxes)
            self.prior_design[k]['feature_sizes_wh'] = (feature_size_w, feature_size_h)
            boxes.extend(this_feature_boxes)
        # back to torch land
        priors_out = torch.Tensor(boxes).view(-1, 4)
        if self.clip:
            if self._logger:
                self._logger.warn('clipping priors! <LEGACY!! probably best if you dont!>')
            priors_out.clamp_(max=1, min=0)
        # print('Total {} priors.'.format(prior_count))
        return priors_out, PriorBoxBase._dumpable_prior_design(self.prior_design, self.image_size)


