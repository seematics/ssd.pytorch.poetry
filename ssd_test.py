import time
from argparse import ArgumentParser
from copy import copy
from logging import ERROR

import cv2
import numpy as np
import torch
import yaml
from allegroai import DataView, DataPipe, Task, InputModel, IterationOrder
from allegroai.debugging import Timer
from allegroai.utilities.plotly import SeriesInfo
from allegroai_api.services import tasks

from common import TIMERS_BENCHMARK_WINDOW
from common.metrics import BoxesHistory, average_precision_from_curve
from common.ssd_pipeline import make_ssd_get_item, make_ssd_batcher
from common.utils import setup_keepalive_logger, get_model_url
from common.utils import ssd_output_to_allegro_format, setup_pytorch, ImageSizeTuple, build_ssd, make_deterministic, \
    resolve_resize_strategy
from common.visualization import DrawBoxesAndLabels

# Please see __main__ below.
# =================   GLOBAL NAMES - meant to be overridden by ui, help reuse task id  ==============================

TASK_NAME = 'Test SSD example'
PROJECT_NAME = 'pytorch ssd'

# epsilon for numerical methods:
EPS = np.finfo(float).eps


def get_parser(input_parser=None):
    parser = input_parser or ArgumentParser(
        description='Single Shot MultiBox Detector Test model With Pytorch')

    # (0) Basic config
    parser.add_argument('--max-test-iterations', '--test-size', default=10000, type=int,
                        help='Max number of iterations for test')
    parser.add_argument('--batch-size', default=6, type=int, help='Number of images per batch')
    parser.add_argument('--report-iterations', default=100, type=int, help='Report Iterations')
    parser.add_argument('--report-images-every-n-reports', default=3, type=int, help='Images report frequency')
    parser.add_argument('--num-workers', default=32, type=int, help='Number of workers used for parallel data loading')
    parser.add_argument('--upload-destination', default='s3://allegro-tutorials', type=str,
                        help='Destination to upload debug images and models')
    parser.add_argument('--feature-extraction-type', default='resnet50', type=str, help='Feature extraction network')
    parser.add_argument('--report-debug-info', default=0, type=int, help='Add debug information')

    # (1) Override model design if needed:
    parser.add_argument('--override-conf-thresh', default=0, type=int, help='override specified conf thresh')
    parser.add_argument('--override-nms-thresh', default=0, type=int, help='override specified nms thresh')
    parser.add_argument('--override-input-w', default=0, type=int, help='override specified w')
    parser.add_argument('--override-input-h', default=0, type=int, help='override specified h')

    # (2) Data control
    parser.add_argument('--resize-strategy', type=str, default='bigger_keep_ar',
                        help='Resize strategy performed on all images that do not match network input size')
    parser.add_argument('--source-id', type=str, default=None,
                        help='In case of several sources per frame, only choose this source id to test on')

    # (3) pytorch specific
    parser.add_argument('--cuda', default=1, type=int, help='Use CUDA to train model')

    return parser


def test_model(net, config_params, args, test_dataview, test_mapping, test_draw_func, logger, at_iteration=0,
               cuda_on=True):
    # hardcoded parameters:
    precision_recall_conf_list = [0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 0.99]
    min_precision_for_integrated_metric = 0.7

    try:
        target_image_size = ImageSizeTuple(w=config_params['input_dim_w'], h=config_params['input_dim_h'])
    except KeyError:
        # backwards compat
        target_image_size = ImageSizeTuple(w=config_params['min_dim_w'], h=config_params['min_dim_h'])

    if args.max_test_iterations <= 0 or not test_dataview:
        logger.console('Skipping test, no frames to test')
        return

    # Set net to eval mode for testing
    net.eval()

    logger.console('%s' % str(80 * '-'))
    t_test = time.time()
    iou = [i for i in config_params['test_detection_iou_thresh']]

    iterator = test_dataview.get_iterator()

    if args.source_id:
        source_id = [args.source_id]
        if len(source_id) > 1:
            raise ValueError(
                'Currently we do not support testing with multiple sources,'
                ' sorry. (source were: {})'.format(source_id))
    else:
        source_id = None
    resize_strategy = resolve_resize_strategy(args, logger)

    test_pipe = DataPipe(iterator,
                         min_queue_depth=max(4, args.num_workers // args.batch_size),
                         batch_size=args.batch_size,
                         num_workers=args.num_workers,
                         get_item_fn=make_ssd_get_item(target_width_height=target_image_size, source_id=source_id),
                         collate_fn=make_ssd_batcher(cuda_on),
                         frame_cls_kwargs=dict(target_width_height=target_image_size,
                                               resize_strategy=resize_strategy,
                                               default_source_ids_to_load=source_id))

    # temporary fix - do not drop entire batch if one image fails to load:
    test_pipe._batcher_kwargs.update(dict(drop_batch_on_error=False))

    test_pipe_iterator = test_pipe.get_iterator()
    logger.console('Testing currently loaded weights...')

    step_timer = Timer()
    if args.report_debug_info:
        net.module.reset_timers() if cuda_on else net.reset_timers()

    box_history = BoxesHistory()

    all_batch_boxes = None
    all_batch_scores = None
    report_iter = 0
    img_idx = 0

    # testing out new feature - print out log messages every interval seconds
    still_alive_msg = "Still testing... [iter {iter}]"
    keepalive = setup_keepalive_logger(still_alive_msg.format(iter=report_iter), logger=logger, interval=60.0)

    for test_iter, batch in enumerate(test_pipe_iterator):
        if test_iter > args.max_test_iterations:
            break

        keepalive.message = still_alive_msg.format(iter=report_iter)

        # forward
        step_timer.tic()

        with torch.no_grad():
            input_images = batch.torch_input.images
            detections = net(input_images, conf_thresh=min(precision_recall_conf_list))
        detections = detections.cpu().numpy()

        step_timer.toc()
        all_batch_boxes, all_batch_scores = \
            ssd_output_to_allegro_format(numpy_ssd_output=detections,
                                         conf_thresh=min(precision_recall_conf_list),
                                         img_width=target_image_size.w,
                                         img_height=target_image_size.h)

        # Make sure we have hard rois
        all_ground_truth = tuple(bb for bb in (batch.ground_truth, batch.hard_ground_truth) if bb.size)
        if all_ground_truth:
            all_ground_truth = np.vstack(all_ground_truth)
            box_history.save_frame_data(pred_boxes=all_batch_boxes, pred_scores=all_batch_scores,
                                        gt_boxes=all_ground_truth)
        else:
            all_ground_truth = None

        report_iter += 1
        if report_iter and report_iter % args.report_iterations == 0:
            msg = 'Test iteration %-6d' % report_iter
            logger.console(msg)

            if args.report_debug_info:
                title = '[TEST] batch_size = {}'.format(len(batch.torch_input.images))
                title += 'Window Average Time (size %d)' % TIMERS_BENCHMARK_WINDOW
                train_iter = ' train_iter={}'.format(at_iteration) if at_iteration > 0 else ''

                logger.console("Cumulative: number of gt {gt} || "
                               "number of pred {pred}".format(gt=len(box_history.gt_boxes),
                                                              pred=len(box_history.pred_boxes)))

                ssd_times = net.module.get_average_times() if cuda_on else net.get_average_times()
                logger.report_scalar(title=title, series='step time'+train_iter,
                                     iteration=report_iter, value=step_timer.average_time)
                logger.report_scalar(title=title, series='fw only time'+train_iter,
                                     iteration=report_iter, value=ssd_times['fw_time'])
                logger.report_scalar(title=title, series='detect only time'+train_iter,
                                     iteration=report_iter, value=ssd_times['detect_time'])

                if TIMERS_BENCHMARK_WINDOW > 0 and (
                        report_iter // args.report_iterations) % TIMERS_BENCHMARK_WINDOW == 0:
                    logger.console('Resetting timer averages at iteration %d' % report_iter)
                    step_timer.reset_average()
                    net.module.reset_timers() if cuda_on else net.reset_timers()

        if report_iter and report_iter % (args.report_iterations * args.report_images_every_n_reports) == 0:
            t_images = time.time()
            np_images = batch.np_images.transpose(0, 2, 3, 1)

            draws = test_draw_func(images=np_images,
                                   pred_scores=all_batch_scores,
                                   pred_boxes=all_batch_boxes,
                                   gt_boxes=all_ground_truth,
                                   gt_boxes_labels=batch.ground_truth_labels,
                                   gt_boxes_hard=batch.hard_ground_truth,
                                   gt_boxes_hard_labels=batch.hard_ground_truth_labels)

            draws = [cv2.putText(im, 'TEST', (10, 30), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255))
                     for im in draws]

            for img in draws:
                logger.report_image_and_upload(title='Test - Images', series='test_img_%d' % img_idx,
                                               iteration=at_iteration, matrix=img.astype(np.uint8))
                img_idx += 1

            t_images = time.time() - t_images
            logger.console('Iter %d Time to create test images: %.4f (sec)' % (report_iter, t_images))
            logger.flush()

    keepalive.should_stop = True
    if args.report_debug_info:
        net.module.reset_timers() if cuda_on else net.reset_timers()

    # Report results
    id_to_label = {v: k for k, v in test_mapping.items() if v > 0}
    logger.console("Done testing. Amount of gt {gt} || pred {pred}".format(gt=len(box_history.gt_boxes),
                                                                           pred=len(box_history.pred_boxes)))
    logger.console('Calculating precision and creating report...')

    for iou_thresh in iou:
        pr_curves_per_label = box_history.get_precision_recall_curve(
            iou_threshold=iou_thresh,
            conf_thresholds=precision_recall_conf_list,
            nms_threshold=config_params.get('nms_thres_for_metrics', 0.65))

        all_ap = []
        all_neap = []
        prec_recall_series = []
        conf_fscore_series = []
        simple_title = '{} | IOU=%3.2f' % iou_thresh
        for label_id in pr_curves_per_label:
            pr_curve = pr_curves_per_label[label_id]
            label_str = id_to_label.get(label_id, 'Label %d' % label_id)
            ap = average_precision_from_curve(pr_curve)
            ap_with_min_prec = average_precision_from_curve(pr_curve, min_precision=min_precision_for_integrated_metric)
            min_prec = min_precision_for_integrated_metric
            prec_recall_graph = [(recall, precision) for _, precision, recall in pr_curve.raw_curve]
            conf_fscore_graph = [(conf, ((2 * recall * precision) / (EPS + recall + precision)))
                                 for conf, precision, recall in pr_curve.raw_curve]
            top_fscore = max([x[1] for x in conf_fscore_graph])
            logger.console('AP=%.2f AP_with_min_prec=%.2f' % (ap, ap_with_min_prec))
            prec_recall_series.append(SeriesInfo(
                name=label_str,
                data=prec_recall_graph,
                labels=['Recall=%3.2f, Precision=%3.2f' % (x[0], x[1]) for x in prec_recall_graph]
            ))
            conf_fscore_series.append(SeriesInfo(
                name=label_str,
                data=conf_fscore_graph,
                labels=['conf_thr=%3.2f' % x[0] for x in conf_fscore_graph]
            ))
            logger.report_scalar(
                title=simple_title.format('Top F1-score'),
                series='%s' % label_str,
                iteration=at_iteration,
                value=top_fscore)
            logger.report_scalar(
                title=simple_title.format('normalized Effective AP_%3.2f' % min_prec),
                series='%s' % label_str,
                iteration=at_iteration,
                value=ap_with_min_prec)
            all_neap.append(ap_with_min_prec)
            logger.report_scalar(
                title=simple_title.format('Average Precision'),
                series='%s' % label_str,
                iteration=at_iteration,
                value=ap)
            all_ap.append(ap)

            logger.console(simple_title.format('Precision-Recall'))
            logger.console(prec_recall_graph)
            logger.console('-------------------------------------------------')
            logger.console(simple_title.format('Conf-F1-score'))
            logger.console(conf_fscore_graph)
            logger.console('-------------------------------------------------')

        logger.report_line_plot(
            title="Precision-Recall @IOU:%s" % iou_thresh,
            series=prec_recall_series,
            iteration=at_iteration,
            xaxis='Recall',
            yaxis='Precision')

        logger.report_line_plot(
            title="Conf-F1-score @IOU:%s" % iou_thresh,
            series=conf_fscore_series,
            iteration=at_iteration,
            xaxis='Conf. Threshold',
            yaxis='F1-Score')

        logger.report_scalar(
            title=simple_title.format('normalized Effective AP_%3.2f' % min_precision_for_integrated_metric),
            series='mnEAP_%3.2f' % min_precision_for_integrated_metric,
            iteration=at_iteration,
            value=np.mean(all_neap))
        logger.report_scalar(
            title=simple_title.format('Average Precision'),
            series='mAP',
            iteration=at_iteration,
            value=np.mean(all_ap))

        logger.flush()

    logger.console('Finished testing snapshot!')
    logger.console('%s' % str(80 * '-'))
    msg = 'Iteration %-6d : test time %.3f (sec)' % (at_iteration, time.time() - t_test)
    logger.console(msg)
    logger.flush()
    test_pipe.terminate()


if __name__ == '__main__':
    ###################################
    # Create task in allegro's system #
    ###################################
    connect_to_task_parser = get_parser()
    task = Task.current_task(default_project_name=PROJECT_NAME, default_task_name=TASK_NAME,
                             default_task_type=tasks.TaskTypeEnum.testing)
    seed = task.get_random_seed()
    make_deterministic(seed)  # setup random seed from task for reproducibility

    #################################
    # Connect arguments to the task #
    #################################
    task.connect(connect_to_task_parser)
    args = connect_to_task_parser.parse_args()
    if args.source_id:
        source_id = [args.source_id]
        if len(source_id) > 1:
            raise ValueError(
                'Currently we do not support testing with multiple sources,'
                ' sorry. (source were: {})'.format(source_id))
    else:
        source_id = None

    logger = task.get_logger()
    logger.set_default_upload_destination(uri=args.upload_destination)
    logger.console('Running arguments: %s' % str(args))

    if hasattr(args, 'test_size'):
        logger.warn('"--test-size" is deprecated.')
        logger.warn('You should use "--max-test-iterations", which will be set to {}'.format(args.test_size))
        args.max_test_iterations = int(args.test_size)

    # (1) Import Model Weights =========================================================================================
    INPUT_MODEL_URL, INPUT_MODEL_NAME = \
        get_model_url(args.feature_extraction_type)

    input_model = InputModel.import_model(weights_url=INPUT_MODEL_URL, name=INPUT_MODEL_NAME,
                                          design=None, label_enumeration=None)
    input_model.publish()
    task.connect(input_model)

    # (2) Setup the output Model labels (mapping to integer IDs) =======================================================
    default_labels = {'hard': -2, 'ignore': -1, 'background': 0,  'person': 1}
    input_model_labels = input_model.labels
    if not input_model_labels or all([value <= 0 for value in input_model_labels.values()]):
        logger.console('No input model labels, or no positive ids : %s , using default labels for this script'
                       % input_model_labels, level=ERROR)
        input_model_labels = default_labels

    current_task_labels = input_model_labels
    positive_model_labels = [v for _, v in current_task_labels.items() if v > 0]
    model_num_positive_classes = len(positive_model_labels)
    if not model_num_positive_classes:
        raise ValueError('No positive label id defined for the model? currently : %s' % str(current_task_labels))
    elif model_num_positive_classes != max(positive_model_labels):
        raise ValueError('Not all model labels have a defined id, cannot run task. num id >0 : '
                         '%d while largest id is %d' % (model_num_positive_classes, max(positive_model_labels)))

    # (3) Dataviews ====================================================================================================
    test_dataview = DataView(iteration_order=IterationOrder.sequential, iteration_infinite=False)
    test_dataview.add_query(dataset_name='PASCAL Visual Object Classes',
                            version_name='Test2012 version', roi_query='person')
    task.connect(test_dataview)

    # == (4) Mapping ===========================================================================================
    label_to_id_mapping_for_test = copy(current_task_labels)
    # make sure we have 'hard' id
    label_to_id_mapping_for_test['hard'] = -2
    # map all difficult persons to hard
    test_dataview.add_mapping_rule(dataset_name='PASCAL Visual Object Classes',
                                   version_name='Test2012 version',
                                   from_labels=['person', 'difficult'], to_label='hard')

    test_dataview.set_labels(label_to_id_mapping_for_test)

    # == (4.3) Apply mapping to debug images ===========================================================================
    id_to_label_mapping_for_test = {v: k for k, v in label_to_id_mapping_for_test.items()}

    # (5) Build Model (pytorch specific from here on) ==================================================================
    # As above - changes made in the design at the web application will override the following:
    config_params = yaml.load(task.get_model_design())  # Enable changing network design in the web app
    if args.override_conf_thresh > 0:
        previous_conf = config_params.pop('test_detection_conf_thresh', 'Not Specified')
        config_params['test_detection_conf_thresh'] = float(args.override_conf_thresh)
        logger.console('Override: confidence thresh for metrics is now %d instead of %s' %
                       (config_params['test_detection_conf_thresh'], str(previous_conf)))
    if args.override_nms_thresh > 0:
        previous_nms = config_params.pop('nms_thresh_for_metrics', 'Not Specified')
        config_params['nms_thresh_for_metrics'] = float(args.override_nms_thresh)
        logger.console('Override: NMS thresh for metrics is now %d instead of %s' %
                       (config_params['nms_thresh_for_metrics'], str(previous_nms)))
    dim_key = 'input_dim_' if 'input_dim_w' in config_params else 'min_dim_'
    if args.override_input_w > 0:
        w_key = dim_key+'w'
        previous_w = config_params[w_key]
        config_params[w_key] = args.override_input_w
        logger.console('Override: input width is now %d instead of %d' %
                       (config_params[w_key], previous_w))
    if args.override_input_h > 0:
        h_key = dim_key+'h'
        previous_h = config_params[h_key]
        config_params[h_key] = args.override_input_h
        logger.console('Override: input width is now %d instead of %d' %
                       (config_params[h_key], previous_h))

    try:
        target_image_size = ImageSizeTuple(w=config_params['input_dim_w'], h=config_params['input_dim_h'])
    except KeyError:
        # backwards compat
        target_image_size = ImageSizeTuple(w=config_params['min_dim_w'], h=config_params['min_dim_h'])

    score_thresh = config_params.get('test_detection_conf_thresh', 0.5)
    draw_boxes_with_mapping_for_test = DrawBoxesAndLabels(labels_mapping=id_to_label_mapping_for_test, logger=logger,
                                                          score_threshold=score_thresh)

    # complain but adjust num-classes if there are more model labels than classes
    design_num_classes = config_params.setdefault('num_classes', 1 + model_num_positive_classes)
    if design_num_classes != 1 + model_num_positive_classes:
        logger.console('Model design contained different number of classes than expected', ERROR)
        raise IOError('model and design mismatch. expected %d classes, task defines %d' %
                      (design_num_classes, 1 + model_num_positive_classes))

    cuda_on = setup_pytorch(args=args, logger=logger, suppress_warnings=True)

    variant = 512 if max(target_image_size) >= 400 else 300

    # add accurate timing information
    if args.report_debug_info:
        timer_flag = 'cuda' if cuda_on else 'cpu'
        config_params['benchmark_timers'] = timer_flag

    ssd_net, freeze_n = build_ssd(args, config_params, variant, target_image_size)

    # create priors for ssd_net
    fake_image = torch.zeros([1, 3, target_image_size.h, target_image_size.w], dtype=torch.float32)
    ssd_net.generate_priors(feature_sizes_wh=ssd_net.get_multiscale_feature_sizes(input_image=fake_image))
    # wrap to enable multiple gpus
    net = torch.nn.DataParallel(ssd_net) if cuda_on else ssd_net
    net.eval()

    # (6) (Finally) Load Weights and test ==============================================================================

    weights_file = input_model.get_weights()
    ssd_net.forgiving_load(weights_file, init_func_on_skip=None, logger=logger, net_type=args.feature_extraction_type)

    # test_model1()
    test_model(net, config_params, args, test_dataview,
               test_mapping=label_to_id_mapping_for_test, test_draw_func=draw_boxes_with_mapping_for_test,
               logger=logger, at_iteration=0, cuda_on=cuda_on)
