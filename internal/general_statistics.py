from copy import copy

from allegroai import DataView, DataPipe, ImageFrame
import numpy as np
import matplotlib.pyplot as plt
import pandas
import tqdm
from abc import ABC, abstractmethod

from collections import namedtuple

from allegroai.utilities.datapipes import ScanningDataPipe

DEFAULT_NUM_SAMPLES = 20
w_h_label_namedtuple = namedtuple('w_h_label_namedtuple', 'w h label')


class GeneralStatistics(ABC):
    WH_LABEL = 'w_h_label_tup'

    def __init__(self, dataview,
                 num_of_samples=DEFAULT_NUM_SAMPLES,
                 default_atomic_data='w_h_label_tup', kwargs=None):
        self.__dataview = dataview
        self.num_of_samples = num_of_samples
        self.label_enumeration = self.__dataview.get_labels()
        if self.label_enumeration:
            self.relevant_labels = list(self.label_enumeration.keys())  # list of labels in the datafram
        self.data_dict = dict()
        self.can_collect = default_atomic_data == self.WH_LABEL
        self._frame_kwargs = kwargs or {}

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return 'Gal Make A Repr ask Roee'

    @staticmethod
    def _extract_roi_w_h_label(current_box):
        """ extracting the width, hight and label of a given RoI.
        returns a list, contains a tuple of the form (width, hight, label) """
        width = current_box['box'][2] - current_box['box'][0]
        height = current_box['box'][3] - current_box['box'][1]
        # label = current_box['labels']['name']
        label = current_box['labels']['id']

        return [w_h_label_namedtuple(width, height, label)]

    @staticmethod
    def _generic_extract_im_data(image_frame, box_extract_fn=_extract_roi_w_h_label):
        """
        :param im: allegro's image object
        :param box_extract_fn: function that extract data from an image RoI,
         (which referred as a box)
        :return:
        """
        im_data_lst = list()
        for box in image_frame.get_boxes():
            im_data_lst += box_extract_fn(box)
        return im_data_lst

    # @staticmethod
    # def _lst_to_np(lst):
    #     # TODO: check
    #     if len(lst) == 0:
    #         return np.zeros(0)
    #     if isinstance(lst[0].int) or isinstance(lst[0], float):
    #         return np.asarray(lst)
    #     for sub_lst in lst:
    #         return GeneralStatistics._lst_to_np(sub_lst)

    @staticmethod
    def print_hist_to_screen(hist, multiple=False):
        def print_hist_subfunction(hist):
            plt.hist(hist, 'auto')
            # plt.hist(hist[0], len(hist))
            # plt.bar(hist[1], hist[0])
            plt.xlabel('value')
            plt.ylabel('Prevalent')
            plt.show()

        if not multiple:
            print_hist_subfunction(hist)
        else:
            # hist is a dictionary with values of lists
            print_hist_subfunction(list(hist.values()))

    @staticmethod
    def add_to_statistic_dict(dictionary, tup, w_h_label_tup_funct):
        statistic = w_h_label_tup_funct(tup)
        # dictionary[tup[2][0]] = dictionary.get(tup[2][0], list())
        # if tup[2][0] not in dictionary:
        #     dictionary[tup[2][0]] = list()
        # dictionary[tup[2][0]].append(statistic)
        if tup[2] not in dictionary:
            dictionary[tup[2]] = list()
        dictionary[tup[2]].append(statistic)

    @staticmethod
    def make_reduce_statistic_dict(statistic_dict, reduce_statistic_func):
        """
        :param statistic_dict: dictinary with list of statistics as the value for every key
        :param reduce_statistic_func: reduce function to apply on each value of the statistic_dict
        :return: a dictionary. keys: the same as the statistic_dict keys. values: the output of the
        reduce function on the original values.
        """
        reduced_statistic_dict = dict()
        for key, value_lst in statistic_dict.items():
            reduced_statistic_dict[key] = reduce_statistic_func(value_lst)
        return reduced_statistic_dict

    @staticmethod
    def generic_statistic(w_h_labels_tup_lst, w_h_label_tup_funct,
                          reduce_statistic_func, label_lst=None, per_label=True):
        """
        :param w_h_labels_tup_lst: a list of width_hight_label tuples
        :param w_h_label_tup_funct:  a function that get a w_h_labels_hist and
        return the wanted statistic - for example - aspect ratio
        :param reduce_statistic_func: a reduce function to apply on the statistic of every key.
        see 'make_reduce_statistic_dict' method documentation for farther detailing.
        :param label_lst: list of relevant label. If not given,
         all the labels in the __dataview are considered relevant.
        :param per_label: if True, the statistic is calculated separately for each label.
        if False, the statistic is calculated for all the labels combined.
        :return: the output of the reduce function on the given input. If per_label = True,
        the output is given as a dictionary
        """

        def is_label_relevant_factory(label_lst):
            """
            :param label_lst: list of label. Can be None.
            :return: a function that takes a label and a label-list, and returns
            a boolean indicates if the label is relevant. If label_lst is None, the returns function
            always returns True.
            """
            if label_lst:
                return lambda label, label_list: label in label_list
            return lambda label, label_list: True  # label in self.relevant_labels

        if per_label:
            statistic_data_structure = dict()
        else:
            statistic_data_structure = w_h_labels_tup_lst()
        is_label_relevant = is_label_relevant_factory(label_lst)
        for tup in w_h_labels_tup_lst:
            # if not is_label_relevant(tup[2][0], label_lst):  # would using label set be faster?
            if not is_label_relevant(tup[2], label_lst):  # would using label set be faster?

                continue
            if per_label:
                GeneralStatistics.add_to_statistic_dict(statistic_data_structure, tup, w_h_label_tup_funct)
            else:
                statistic_data_structure.append(w_h_label_tup_funct(tup))
        if per_label:
            reduced_statistic = GeneralStatistics.make_reduce_statistic_dict(statistic_data_structure,
                                                                             reduce_statistic_func)
        else:
            reduced_statistic = reduce_statistic_func(statistic_data_structure)
        # print the histogram to screen
        if reduce_statistic_func == GeneralStatistics.hist:
            GeneralStatistics.print_hist_to_screen(statistic_data_structure, multiple=per_label)
        return reduced_statistic  # np.histogram(statistic_lst, bins='auto')

    #####################
    #  w_h_label_funct  #
    #####################

    @staticmethod
    def _extract_aspect_ratio_and_area(w_h_label_tuple):
        return w_h_label_tuple[0] / w_h_label_tuple[1]

    @staticmethod
    def _extract_area(w_h_label_tuple):
        return w_h_label_tuple[0] * w_h_label_tuple[1]

    @staticmethod
    def _extract_w(w_h_label_tuple):
        return w_h_label_tuple[0]

    @staticmethod
    def _extract_h(w_h_label_tuple):
        return w_h_label_tuple[1]

    ##########################
    # reduce_statistic_funct #
    ##########################

    @staticmethod
    def hist(statistic_lst):
        return np.histogram(statistic_lst, bins='auto')

    @staticmethod
    def mean(statistic_lst):
        return np.mean(statistic_lst)

    def create_data_lst(self, extract_image_data_fn, extract_box_data_fn, frame_cls_kwargs=None):
        """ create a list of width_hight_label tuples from the __dataview"""
        data_list = list()

        if 'crop_width' in frame_cls_kwargs.keys():
            default_overlap_ratio = 0.1
            frame_cls_kwargs_for_crop = copy(frame_cls_kwargs)
            frame_cls_kwargs_for_crop.setdefault('overlap_percent', default_overlap_ratio)
            pipe = ScanningDataPipe(iterator=self.__dataview.get_iterator(), frame_cls_kwargs=frame_cls_kwargs_for_crop)
        else:
            pipe = DataPipe(iterator=self.__dataview.get_iterator(), frame_cls_kwargs=frame_cls_kwargs)

        iterator = tqdm.tqdm(pipe.get_iterator(), total=self.num_of_samples, desc='Collecting data...')
        for index, im in enumerate(iterator):
            data_list += extract_image_data_fn(im, extract_box_data_fn)
            # for box in im.get_boxes():
            #     data_list.append(image_extract_fn(box,))
            if index > self.num_of_samples:
                iterator.close()
                break
        else:
            # TODO - this wont work. add proper warning
            Warning('Small dataset? early exit')

        return data_list  # np.array(data_list)

    # def calc_area_hist(self, label_lst=None):
    #     self.collect_data()
    #     w_h_label_tup_lst = self.data_dict['w_h_label_tup']
    #     area_hist = GeneralStatistics.generic_statistic(
    #         w_h_label_tup_lst, GeneralStatistics._extract_area,
    #         GeneralStatistics.hist, label_lst=label_lst)
    #     return area_hist

    ###############
    ## properties##
    ###############
    @abstractmethod
    def collect_data(self, refresh=False):
        pass
    # def collect_data(self, refresh=False):
    #     if self.can_collect:
    #         if self.data_dict.get('w_h_label_tup', None) is None or refresh:
    #             print('...collecting data... this may take a while')
    #             self.data_dict['w_h_label_tup'] = self.create_data_lst(GeneralStatistics._generic_extract_im_data,
    #                                                                  GeneralStatistics._extract_roi_w_h_label)
    #             self.data_dict['pandas_w_h_label_array'] = pandas.DataFrame(self.data_dict['w_h_label_tup'], columns=['width', 'height', 'label'])
    #
    #     else:
    #         raise NotImplementedError('Cannot automatically collect for selected data type')
    #
    # @property
    # def width_array(self):
    #     return self.data_dict['pandas_w_h_label_array']['width']
    #
    # @property
    # def height_array(self):
    #     return self.data_dict['pandas_w_h_label_array']['height']
    #
    # @property
    # def label_id_array(self):
    #     return self.data_dict['pandas_w_h_label_array']['label']
    #
    # @property
    # def as_pandas_width_height_label_array(self):
    #     return self.data_dict['pandas_w_h_label_array']


if __name__ == '__main__':
    __dataview = DataView()
    # __dataview.add_query(dataset_name='PASCAL Visual Object Classes', version_name='Train2012 version')
    __dataview.add_query(dataset_name='PASCAL Visual Object Classes', version_name='Train2012 version',
                       roi_query='car', weight=2.0)
    __dataview.add_query(dataset_name='PASCAL Visual Object Classes', version_name='Train2012 version',
                       roi_query='person', weight=2.0)
    # __dataview.set_labels(labels_enumeration)
    # task = Task.current_task(default_project_name="My project", default_task_name="My task")
    # logger = task.get_logger()
    # logger.set_default_upload_destination('/galh/Documents/logs')
    general_statistics = GeneralStatistics(__dataview)
    label_lst = [7, 15]  # ['car', 'person']
    area_hist = general_statistics.calc_area_hist(label_lst=label_lst)
    print(area_hist)
    #general_statistics.print_hist_to_screen(area_hist)

    # statisti = general_statistics.calculate_wanted_statistic(data_extraction_fn = ob._im_data,
    #                                          calc_statistic_fn = ob._histograms,  # ob._avg_roi_coordinates,
    #                                    image_extract_fn = ob._width_and_hight)
    # print(statisti)

#     ####################################################################################

#
#     def calculate_wanted_statistic(self, data_extraction_fn,
#                                    calc_statistic_fn,
#                                    image_extract_fn=None,
#                                    ):
#
#         pipe = DataPipe(self.__dataview.get_iterator())
#         data_list = data_extraction_fn(pipe, image_extract_fn)
#         statistic = calc_statistic_fn(data_list)
#         return statistic
#
#     def print_to_json(self, statistic, statistic_name):
#         json.dumps({statistic: statistic_name})
#
#     def _im_data(self, pipe, image_extract_fn):
#         data_matrix = list()
#         for index, im in enumerate(pipe.get_iterator()):
#             for index, box in enumerate(im.get_boxes()):
#                 data_matrix.append(image_extract_fn(box))
#             if index > self.num_of_samples:
#                 break
#         return np.array(data_matrix)
#
#     ##############################
#     ####cal statistic function####
#     ##############################
#
#     def _avg_roi_coordinates(self, coordinate_list):
#         mean = np.mean(coordinate_list, axis=0)
#         return mean
#
#     def _histograms(self, width_n_hight_list):
#         # width_n_hight_hist = list()
#         num_of_bins = self.num_of_samples // 2
#         area_list = np.zeros(len(width_n_hight_list))
#         aspect_ration_list = np.zeros(len(width_n_hight_list))
#         for index, width_n_hight in enumerate(width_n_hight_list):
#             area_list[index] = width_n_hight[0] * width_n_hight[1]
#             aspect_ration_list[index] = width_n_hight[0] / width_n_hight[1]
#         width_hist = np.histogram(width_n_hight_list[:, 0], num_of_bins)
#         hight_hist = np.histogram(width_n_hight_list[:, 1], num_of_bins)
#         area_hist = np.histogram(area_list, num_of_bins)
#         aspect_ration_hist = np.histogram(aspect_ration_list, num_of_bins)
#         return width_hist, hight_hist, area_hist, aspect_ration_hist
#
#     ##############################
#     ##image extraction functions##
#     ##############################
#
#     def _box_coordinate(self, current_box):
#         return np.array(current_box['box'])
#
#     def _width_and_hight(self, current_box):
#         width = current_box['box'][2] - current_box['box'][0]
#         hight = current_box['box'][3] - current_box['box'][1]
#         # res = np.zeros(2)
#         # res[0] = width
#         # res[1] = hight
#         # return res
#         return np.array([width, hight])
#
# #
# # if __name__ == '__main__':
# #     __dataview = DataView()
# #     __dataview.add_query(dataset_name='PASCAL Visual Object Classes', version_name='Train2012 version')
# #     # task = Task.current_task(default_project_name="My project", default_task_name="My task")
# #     # logger = task.get_logger()
# #     # logger.set_default_upload_destination('/galh/Documents/logs')
# #     ob = GeneralStatistics(__dataview)
# #     statisti = ob.calculate_wanted_statistic(data_extraction_fn = ob._im_data,
# #                                              calc_statistic_fn = ob._histograms,  # ob._avg_roi_coordinates,
# #                                        image_extract_fn = ob._width_and_hight)
# #     print(statisti)
