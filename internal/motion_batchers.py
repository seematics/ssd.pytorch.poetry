import numpy as np
from collections import namedtuple
from common.utils import normalize_boxes

from allegroai import ImageFrame

motion_input = namedtuple('MotionInput', ['image_frame', 'motion_mat'])
ssd_batch = namedtuple('SSDBatch', ['input', 'targets', 'ground_truth', 'hard_ground_truth', 'size',
                                    'ground_truth_labels', 'hard_ground_truth_boxes', 'hard_ground_truth_labels'])

HARD_LABEL_NAMES = {'hard', '__hard__'}


def make_ssd_batch_with_motion(image_motion_input_tuple_batch, target_image_size_wh, color_channels=3,
                               motion_channels=None, dtype=np.float32, source_id=None):
    if source_id:
        raise NotImplementedError('this file was not updated in a long long time, will not work this way')
    motion_channels = motion_channels or color_channels
    targets = []
    batch_rois = []
    batch_labels = []
    batch_labels_names = []
    batch_hard_rois = []
    batch_hard_rois_label_names = []
    batch_hard_rois_list = []

    list_of_img = []
    list_of_motion_mat = []

    for bidx, (image_frame, motion_mat) in enumerate(image_motion_input_tuple_batch):
        polygons = image_frame.get_polygons()
        labels = []
        labels_names = []
        hard_rois = []
        hard_rois_labels_names = []
        rois = []
        pos_label_count = 0
        for roi in polygons:
            label_num = roi['labels'].get('id', -1)
            label_name = roi['labels'].get('name', ['BadLabel'])

            box = np.maximum(ImageFrame.get_polygon_bounding_box(roi['points']), 0)

            if label_num >= 0:
                pos_label_count += 1
                labels.append(label_num)
                rois.append(box)
                labels_names.append(label_name)
            elif label_num < -1 or HARD_LABEL_NAMES.intersection(set(label_name)):
                # A hard label is used to designate rois for which matching does not constitute a false positive
                # It is entirely left for the user to defined mapping rules to change the labels to either one of
                # HARD_LABEL_NAMES or the id's to smaller than -1 (previous default was id==-2 for hard)
                hard_rois.append(box)
                hard_rois_labels_names.append(label_name)

        # always collect the hard label names (so the index is the same)
        batch_labels_names.append(labels_names)
        batch_hard_rois_label_names.append(hard_rois_labels_names)
        batch_hard_rois_list.append(hard_rois)

        # collect the hard rois
        if hard_rois:
            # create batch index column
            batch_hard_rois.append(np.hstack([np.ones((len(hard_rois), 1)) * bidx, np.array(hard_rois)]))

        labels = np.array(labels)
        if labels.size:
            valid_idx = np.where(labels >= 0)[0]
            rois = np.vstack(rois)[valid_idx, :]
            labels = labels[valid_idx]

            normalized_rois = normalize_boxes(rois, img_h=target_image_size_wh[1], img_w=target_image_size_wh[0])
            normalized_boxes_with_labels = np.hstack([normalized_rois, labels[:, np.newaxis]]).astype(np.float32)
            targets.append(normalized_boxes_with_labels)
            batch_rois.append(np.hstack([np.ones((rois.shape[0], 1)) * bidx, rois]))
            batch_labels.append(labels)
        else:
            # need a very tiny box of background to work with current implementation
            targets.append(np.atleast_2d(np.array([0, 0, 0.001, 0.001, 0], dtype=dtype)))

        # check if the motion map is actually a tuple of transformed image and motion mat
        if not isinstance(motion_mat, np.ndarray):
            list_of_img.append(motion_mat[0].transpose(2, 0, 1))
            list_of_motion_mat.append(motion_mat[1].transpose(2, 0, 1))
        else:
            list_of_img.append(image_frame.get_data().transpose(2, 0, 1))
            list_of_motion_mat.append(motion_mat.transpose(2, 0, 1))

    batch_size = len(image_motion_input_tuple_batch)
    img_tensor = np.zeros((batch_size, color_channels) + tuple(target_image_size_wh)[::-1], dtype=dtype)
    motion_tensor = np.zeros((batch_size, motion_channels) + tuple(target_image_size_wh)[::-1], dtype=dtype)

    for bidx, (im, mot) in enumerate(zip(list_of_img, list_of_motion_mat)):
        if im is not None:
            # d1, d2, d3 are dimensions w,h,ch (! - might appear in any order)
            d1, d2, d3 = im.shape
            img_tensor[bidx, :d1, :d2, :d3] = im
        if mot is not None:
            # d1, d2, d3 are dimensions w,h,ch (! - might appear in any order)
            d1, d2, d3 = mot.shape
            motion_tensor[bidx, :d1, :d2, :d3] = mot

    batch_rois = np.vstack(batch_rois) if len(batch_rois) else np.empty([0, 4])
    batch_labels = np.concatenate(batch_labels) if len(batch_labels) else np.array([])
    batch_gt_rois_labels = np.hstack([batch_rois, batch_labels[:, np.newaxis]]) if (
            batch_rois.size and batch_labels.size) else np.empty(shape=[0, 6])

    # create the hard rois in the same format as the rest of the boxes (rois/label)
    batch_hard_rois = np.vstack(batch_hard_rois) if len(batch_hard_rois) else np.empty([0, 4])
    batch_hard_labels = np.ones(shape=(batch_hard_rois.shape[0], 1)) * -2
    batch_hard_gt_rois_lables = np.hstack((batch_hard_rois, batch_hard_labels)) if (
            batch_hard_rois.size and batch_hard_labels.size) else np.empty(shape=[0, 6])

    motion_batch = motion_input(image_frame=img_tensor, motion_mat=motion_tensor)
    return ssd_batch(motion_batch, targets, batch_gt_rois_labels, batch_hard_gt_rois_lables, batch_size,
                     ground_truth_labels=batch_labels_names,
                     hard_ground_truth_boxes=batch_hard_rois_list,
                     hard_ground_truth_labels=batch_hard_rois_label_names)


