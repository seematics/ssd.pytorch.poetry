from allegroai import DataView, DataPipe, ImageFrame
import numpy as np
import matplotlib.pyplot as plt
import pandas

from collections import namedtuple
from .general_statistics import GeneralStatistics
from allegroai.utilities.common_dataset_labels import VOC_LABELS

DEFAULT_NUM_SAMPLES = 20
labels_enumeration = {key: val[0] for key, val in VOC_LABELS.items() if val[0] > 0}

w_h_label_namedtuple = namedtuple('w_h_label_namedtuple', 'w h label')


class AreaStatistics(GeneralStatistics):

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return 'area statistics object: ' +\
               super.__repr__()  #''Gal Make A Repr ask Roee'

    def collect_data(self, refresh=False):
        if self.can_collect:
            if self.data_dict.get('w_h_label_tup', None) is None or refresh:
                self.data_dict['w_h_label_tup'] = self.create_data_lst(GeneralStatistics._generic_extract_im_data,
                                                                       GeneralStatistics._extract_roi_w_h_label,
                                                                       frame_cls_kwargs=self._frame_kwargs)
                self.data_dict['pandas_w_h_label_array'] = \
                    pandas.DataFrame(self.data_dict['w_h_label_tup'],
                                     columns=['width', 'height', 'label'])

        else:
            raise NotImplementedError('Cannot automatically collect for selected data type')

    @property
    def width_array(self):
        return self.data_dict['pandas_w_h_label_array']['width']

    @property
    def height_array(self):
        return self.data_dict['pandas_w_h_label_array']['height']

    @property
    def label_id_array(self):
        return self.data_dict['pandas_w_h_label_array']['label']

    @property
    def as_pandas_width_height_label_array(self):
        return self.data_dict['pandas_w_h_label_array']

    @staticmethod
    def _extract_roi_w_h_label(current_box):
        """ extracting the width, hight and label of a given RoI.
        returns a list, contains a tuple of the form (width, hight, label) """
        width = current_box['box'][2] - current_box['box'][0]
        height = current_box['box'][3] - current_box['box'][1]
        # label = current_box['labels']['name']
        label = current_box['labels']['id']

        return [w_h_label_namedtuple(width, height, label)]

    # @staticmethod
    # def _generic_extract_im_data(image_frame, box_extract_fn=_extract_roi_w_h_label):
    #     """
    #     :param im: allegro's image object
    #     :param box_extract_fn: function that extract data from an image RoI,
    #      (which referred as a box)
    #     :return:
    #     """
    #     im_data_lst = list()
    #     for box in image_frame.get_boxes():
    #         im_data_lst += box_extract_fn(box)
    #     return im_data_lst

    @staticmethod
    def print_hist_to_screen(hist, multiple=False):
        def print_hist_subfunction(hist):
            plt.hist(hist, 'auto')
            # plt.hist(hist[0], len(hist))
            # plt.bar(hist[1], hist[0])
            plt.xlabel('value')
            plt.ylabel('Prevalent')
            plt.show()

        if not multiple:
            print_hist_subfunction(hist)
        else:
            # hist is a dictionary with values of lists
            print_hist_subfunction(list(hist.values()))

    # @staticmethod
    # def add_to_statistic_dict(dictionary, tup, w_h_label_tup_funct):
    #     statistic = w_h_label_tup_funct(tup)
    #     # dictionary[tup[2][0]] = dictionary.get(tup[2][0], list())
    #     # if tup[2][0] not in dictionary:
    #     #     dictionary[tup[2][0]] = list()
    #     # dictionary[tup[2][0]].append(statistic)
    #     if tup[2] not in dictionary:
    #         dictionary[tup[2]] = list()
    #     dictionary[tup[2]].append(statistic)

    #
    # @staticmethod
    # def make_reduce_statistic_dict(statistic_dict, reduce_statistic_func):
    #     """
    #     :param statistic_dict: dictinary with list of statistics as the value for every key
    #     :param reduce_statistic_func: reduce function to apply on each value of the statistic_dict
    #     :return: a dictionary. keys: the same as the statistic_dict keys. values: the output of the
    #     reduce function on the original values.
    #     """
    #     reduced_statistic_dict = dict()
    #     for key, value_lst in statistic_dict.items():
    #         reduced_statistic_dict[key] = reduce_statistic_func(value_lst)
    #     return reduced_statistic_dict

    # @staticmethod
    # def generic_statistic(w_h_labels_tup_lst, w_h_label_tup_funct,
    #                       reduce_statistic_func, label_lst=None, per_label=True):
    #     """
    #     :param w_h_labels_tup_lst: a list of width_hight_label tuples
    #     :param w_h_label_tup_funct:  a function that get a w_h_labels_hist and
    #     return the wanted statistic - for example - aspect ratio
    #     :param reduce_statistic_func: a reduce function to apply on the statistic of every key.
    #     see 'make_reduce_statistic_dict' method documentation for farther detailing.
    #     :param label_lst: list of relevant label. If not given,
    #      all the labels in the __dataview are considered relevant.
    #     :param per_label: if True, the statistic is calculated separately for each label.
    #     if False, the statistic is calculated for all the labels combined.
    #     :return: the output of the reduce function on the given input. If per_label = True,
    #     the output is given as a dictionary
    #     """
    #
    #     def is_label_relevant_factory(label_lst):
    #         """
    #         :param label_lst: list of label. Can be None.
    #         :return: a function that takes a label and a label-list, and returns
    #         a boolean indicates if the label is relevant. If label_lst is None, the returns function
    #         always returns True.
    #         """
    #         if label_lst:
    #             return lambda label, label_list: label in label_list
    #         return lambda label, label_list: True  # label in self.relevant_labels
    #
    #     if per_label:
    #         statistic_data_structure = dict()
    #     else:
    #         statistic_data_structure = list()
    #     is_label_relevant = is_label_relevant_factory(label_lst)
    #     for tup in w_h_labels_tup_lst:
    #         # if not is_label_relevant(tup[2][0], label_lst):  # would using label set be faster?
    #         if not is_label_relevant(tup[2], label_lst):  # would using label set be faster?
    #
    #             continue
    #         if per_label:
    #             GeneralStatistics.add_to_statistic_dict(statistic_data_structure, tup, w_h_label_tup_funct)
    #         else:
    #             statistic_data_structure.append(w_h_label_tup_funct(tup))
    #     if per_label:
    #         reduced_statistic = GeneralStatistics.make_reduce_statistic_dict(statistic_data_structure,
    #                                                                          reduce_statistic_func)
    #     else:
    #         reduced_statistic = reduce_statistic_func(statistic_data_structure)
    #     # print the histogram to screen
    #     if reduce_statistic_func == GeneralStatistics.hist:
    #         GeneralStatistics.print_hist_to_screen(statistic_data_structure, multiple=per_label)
    #     return reduced_statistic  # np.histogram(statistic_lst, bins='auto')

    #####################
    ## w_h_label_funct ##
    #####################

    @staticmethod
    def _extract_aspect_ratio_and_area(w_h_label_tuple):
        return w_h_label_tuple[0] / w_h_label_tuple[1]

    @staticmethod
    def _extract_area(w_h_label_tuple):
        return w_h_label_tuple[0] * w_h_label_tuple[1]

    @staticmethod
    def _extract_w(w_h_label_tuple):
        return w_h_label_tuple[0]

    @staticmethod
    def _extract_h(w_h_label_tuple):
        return w_h_label_tuple[1]

    ##########################
    ##reduce_statistic_funct##
    ##########################

    @staticmethod
    def hist(statistic_lst):
        return np.histogram(statistic_lst, bins='auto')

    @staticmethod
    def mean(statistic_lst):
        return np.mean(statistic_lst)

    # def create_data_lst(self, extract_image_data_fn, extract_box_data_fn):
    #     """ create a list of width_hight_label tuples from the __dataview"""
    #     data_list = list()
    #     pipe = DataPipe(self.__dataview.get_iterator())
    #     for index, im in enumerate(pipe.get_iterator()):
    #         data_list += extract_image_data_fn(im, extract_box_data_fn)
    #         # for box in im.get_boxes():
    #         #     data_list.append(image_extract_fn(box,))
    #         if index > self.num_of_samples:
    #             break
    #     return data_list  # np.array(data_list)

    def calc_area_hist(self, label_lst=None):
        self.collect_data()
        w_h_label_tup_lst = self.data_dict['w_h_label_tup']
        area_hist = GeneralStatistics.generic_statistic(
            w_h_label_tup_lst, GeneralStatistics._extract_area,
            GeneralStatistics.hist, label_lst=label_lst)
        return area_hist


if __name__ == '__main__':
    __dataview = DataView()
    # __dataview.add_query(dataset_name='PASCAL Visual Object Classes', version_name='Train2012 version')
    __dataview.add_query(dataset_name='PASCAL Visual Object Classes', version_name='Train2012 version',
                       roi_query='car', weight=2.0)
    __dataview.add_query(dataset_name='PASCAL Visual Object Classes', version_name='Train2012 version',
                       roi_query='person', weight=2.0)
    __dataview.set_labels(labels_enumeration)
    # task = Task.current_task(default_project_name="My project", default_task_name="My task")
    # logger = task.get_logger()
    # logger.set_default_upload_destination('/galh/Documents/logs')
    general_statistics = AreaStatistics(__dataview)
    label_lst = [7, 15]  # ['car', 'person']
    area_hist = general_statistics.calc_area_hist(label_lst=label_lst)
    print(area_hist)
