"""
Selects which boxes out of a (concatenated) list of boxes should be used to calculate each loss type
 (supports confidence and localization loss)
Backwards makes sure loss for data etc..

dev notes:

AJB - should we really be inputting number of classes here? we can infer it from the input blobs.

"""

from seematics import caffe
from sys import platform
import numpy as np
import yaml

DEBUG = False


def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum()


def bboxes_area(boxes):
    return (boxes[:, 2] - boxes[:, 0]) * (boxes[:, 3] - boxes[:, 1])


class SelectGoodBoxesLayer(caffe.Layer):
    """
    select good boxes: funnels the SSD (or similar) output so that loss can be calculated on only a certain number of
     "good" boxes, while choosing specific boxes to serve as fodder for the background class
    NEW: now with padding for constant BLOB out size
    (FW:)
    param_strings that can be controlled from input:




    inputs:



        bottom 0 blob[batch_size,1,num_priors,num_classes] of confidence from network
        bottom 1 blob[batch_size,1,num_priors,4] of regressor output from network
        bottom 2 blob[batch_size,2,num_priors,4] of encoded gt_boxes for each prior box that touched the gt_box
         (the rest get boxisnull from boxutils) ((note,  second channel contains the original (normalized) gt_boxes))
        bottom 3 blob[batch_size,1,1,num_priors] of matched labels (dtype) for each prior box that was matched
         (the rest get default label <-1)
        bottom 4 blob[batch_size,1,num_priors] of the IOU overlap between the prior and the gt_box that it was matched
         to, note that you can pass any score
        //optional:
        bottom 5 blob[2,num_priors,4] the priors and their variance parameters -
            (for debug, should be reshaped beforehand to be 0,-1,4 like bottom[2])
    outputs:



        top 0 blob[num_boxes_conf,num_classes] of selected confidences from the network, all images in single blob
        top 1 blob[num_boxes_conf,1] of correct labels matching top 0 all in single blob
        top 2 blob[num_boxes_loc,4] of selected regressor output from the network all in single blob
        top 3 blob[num_boxes_loc,4] of encoded gt_boxes so that L1_dist(top2,top3) is the localization loss
        //optional (useful)
        top 4 blob[num_boxes_loc] the batch index of each row in the previous (and following) blobs
        //optional blobs (if bottom 5 blob is present)
        top 5 blob[2,num_boxes_loc,4] the prior box of each selected box(and their variance param in the next channels)
        top 6 blob[num_boxes_loc,4] the gt box corresponding to the top 3 blob
        //optional blob if present
        top 7 blob[num_boxes_loc,num_classes] very special to blob.
    """

    def __init__(self, *args):
        super(SelectGoodBoxesLayer, self).__init__(*args)
        # will hold numpy array of indices
        self._conf_index = []
        self._loc_index = []
        # the rest of caffe needs to know the widths etc of our outputs, so we have a dummy for the first run
        self._init_reshape = False
        self._prior_output_mode = False
        self._index_blob_on = False
        self._gt_box_output_on = False
        self._loc_conf_output_on = False
        #
        self._max_conf_out = 0
        self._max_loc_out = 0
        # boost number of bg chosen if there are few fg
        self._boost_bg_if_fg_count_low = 0.1
        #
        self._reweight_loc_loss_because_padding = 1.0
        self._reweight_conf_loss_because_padding = 1.0
        #
        self._location_of_stat_blob = None
        # for debug:
        self._debug_indexes = None
        self._debug_labels = None
        self._debug_scores = None

    def setup(self, bottom, top):

        # check number of bottom blobs
        assert (len(bottom) == 5) or (len(bottom) == 6)
        assert (len(top) >= 4)

        if len(top) > 5:
            self._index_blob_on = True

        # check number of top blobs and activate debug mode if needed
        if len(bottom) == 6:
            # you can put an extra bottom blob in but if there is no extra top nothing will happen
            if len(top) > 5:
                self._prior_output_mode = True
            if len(top) > 6:
                self._gt_box_output_on = True
            if len(top) > 7:
                self._loc_conf_output_on = True

        # setup input parameters (validation via lambda)
        layer_params = yaml.safe_load(self.param_str) if self.param_str else {}
        input_flags = {
            # master flag to override many of the flags below
            'continuous_learning': (False, lambda x: isinstance(x, bool)),
            # used to check dimensions
            'num_classes': (21, lambda x: x > 0),
            # threshold for determining what is a good gt match
            'fg_iou_thresh': (0.3, lambda x: x >= 0),
            # threshold for determining what is a good gt match
            'hard_bg_max_iou': (0.3, lambda x: x >= 0),
            # deprecated:
            'similarity_thresh': (0, lambda x: isinstance(x, (int, float)) and x >= 0),
            # boost similarity threshold for hw towards shrinking
            'hw_similarity_prefer_smaller_gt': (False, lambda x: isinstance(x, bool)),
            # box similarity outlier detection - within the chosen encoding
            'hw_similarity_thresh': (1.5, lambda x: isinstance(x, (int, float)) and x >= 0),
            # box similarity outlier detection - within the chosen encoding
            'xy_similarity_thresh': (0, lambda x: isinstance(x, (int, float)) and x >= 0),
            # if set, no more than max_num_... will be used for training"
            'max_num_fg_box_per_image': (300, lambda x: x is None or (isinstance(x, int) and x > 0)),
            # fixes the ratio between all bg box and number of fg-matched boxes
            'bg_to_fg_box_ratio': (2, lambda x: x >= 0),
            # percentage of "hard bg" examples to take (hard = actually matched with gt_box but below threshold)
            'hard_bg_mix': (0.3, lambda x: x >= 0),
            # percentage of "false_positive" examples to take (easy bg candidates with atypically large objecntess)
            'false_pos_mix': (0.5, lambda x: x >= 0),
            # pick hardest background "false_positive" samples (lowest scores for background class) rather than easy
            'pick_hard_false_pos': (False, lambda x: isinstance(x, bool)),
            # specifically ignore false_positives
            'ignore_false_pos': (False, lambda x: isinstance(x, bool)),
            # how many positives to drop
            'ignore_false_pos_conf_thresh': (0.5, lambda x: isinstance(x, (int, float)) and x >= 0),
            # use pick 'hard examples' for background "false_positive" samples
            'hard_example_mining': (True, lambda x: isinstance(x, bool)),
            # deprecated - same flag as 'hard_example_mining'
            'use_softmax': (True, lambda x: isinstance(x, bool)),
            # map all the bg examples so their gt label is this
            'background_class_index': (0, lambda x: isinstance(x, int)),
            # deprecated: easy bg will be those that do not match with any box (including ignored boxes)
            'easy_bg_can_match_with_ignored_box': (False, lambda x: isinstance(x, bool)),
            # maximum iou by which ignore ground-truth can be matched with an easy bg
            'easy_bg_max_iou_with_ignore': (0, lambda x: x >= 0),
            # easy bg will take largest possible boxes, otherwise - random.
            'prefer_largest': (False, lambda x: isinstance(x, bool)),
            # debug_boxes packed output
            'top_9_debug_priors_out': (False, lambda x: isinstance(x, bool)),
            # report prior match
            'report_prior_match': (False, lambda x: isinstance(x, bool)),
        }

        flag_values = {k: layer_params.get(k, default) for k, (default, _) in input_flags.items()}
        invalid = [k for k, v in flag_values.items() if not input_flags[k][1](v)]
        if invalid:
            raise ValueError('Invalid values for input flags %s' % ', '.join(invalid))

        for k, v in flag_values.items():
            setattr(self, '_%s' % k, v)


        # overrides:
        if self._continuous_learning:
            self._ignore_false_pos = True
            self._hard_example_mining = True

        if self._ignore_false_pos:
            self._false_pos_mix = 0.0

        # deprecated keywords conversion:
        if self._use_softmax and not self._hard_example_mining:
            self._hard_example_mining = True

        if self._easy_bg_can_match_with_ignored_box and self._easy_bg_max_iou_with_ignore == 0:
            self._easy_bg_max_iou_with_ignore = 0.1

        if self._similarity_thresh > 0:
            self._hw_similarity_thresh = self._similarity_thresh
            self._similarity_thresh = None

        if self._top_9_debug_priors_out:
            required_len = 10 if self._report_prior_match else 9
            assert len(top) == required_len, "debug bg out rois need a dedicated 9th top"

        # make sure hard bg max iou is <= fg iou
        self._hard_bg_max_iou = min(self._hard_bg_max_iou, self._fg_iou_thresh)
        # make sure no matched box will be also matched to bg
        self._easy_bg_max_iou_with_ignore = min(self._easy_bg_max_iou_with_ignore, self._fg_iou_thresh)

        # set minimal padding for the image:
        self._max_loc_out = self._max_num_fg_box_per_image
        self._max_conf_out = int(self._max_loc_out * (1 + self._bg_to_fg_box_ratio))

        if self._report_prior_match:
            self._location_of_stat_blob = len(top) - 1
            required_place = 8 if not self._top_9_debug_priors_out else 9
            required_place = 4 if not self._index_blob_on else required_place
            assert (self._location_of_stat_blob == required_place), \
                'untested number of top blobs for stats...'

    def reshape(self, bottom, top):

        batch_size = bottom[0].data.shape[0]

        # update padding if batch size is different
        # TODO: update padding sizes based on last fw
        self._max_loc_out = max(self._max_loc_out, batch_size * self._max_num_fg_box_per_image)
        self._max_conf_out = max(self._max_conf_out,
                                 batch_size * int(self._max_num_fg_box_per_image * (1 + self._bg_to_fg_box_ratio)))

        # reset indices
        self._conf_index = [[] for _ in range(batch_size)]
        self._loc_index = [[] for _ in range(batch_size)]
        if self._top_9_debug_priors_out:
            self._debug_indexes = [[] for _ in range(batch_size)]
            self._debug_labels = [[] for _ in range(batch_size)]
            self._debug_scores = [[] for _ in range(batch_size)]

        # TODO : check and verify size of bottom blobs? currently done in forward

        # reshape tops so that they have the required width and other layers would be able to work with them?
        shape_vector_conf = [1, 1, self._max_conf_out, self._num_classes]
        shape_vector_conf_for_loc_conf_output = [self._max_loc_out, self._num_classes]
        shape_vector_conf_label = [1, 1, 1, self._max_conf_out]
        shape_vector_loc = [self._max_loc_out, 4]
        top[0].reshape(*shape_vector_conf)
        top[1].reshape(*shape_vector_conf_label)
        top[2].reshape(*shape_vector_loc)
        top[3].reshape(*shape_vector_loc)
        if self._index_blob_on:
            shape_vector_index_label = [1, 1, 1, self._max_loc_out]
            top[4].reshape(*shape_vector_index_label)
        if self._prior_output_mode:
            shape_vector_prior = [2, self._max_loc_out, 4]
            top[5].reshape(*shape_vector_prior)
        if self._gt_box_output_on:
            shape_vector_gtbox = [self._max_loc_out, 4]
            top[6].reshape(*shape_vector_gtbox)
        if self._loc_conf_output_on:
            top[7].reshape(*shape_vector_conf_for_loc_conf_output)

        pass

    def forward(self, bottom, top):

        # TODO - raise and write explicit explanation on what is wrong
        # check and verify sizes of bottom blobs
        batch_size = bottom[0].data.shape[0]
        should_be_num_classes = bottom[0].data.shape[-1]
        assert should_be_num_classes == self._num_classes, str(should_be_num_classes)
        num_priors = bottom[0].data.shape[-2]

        should_be_num_priors = [bottom[3].data.shape[-1], bottom[4].data.shape[-1]]
        assert all(x == num_priors for x in should_be_num_priors), str(should_be_num_priors)

        should_be_num_priors_too = [bottom[1].data.shape[-2], bottom[2].data.shape[-2]]
        assert all(x == num_priors for x in should_be_num_priors_too), str(should_be_num_priors_too)

        should_be_4 = [bottom[1].data.shape[-1], bottom[2].data.shape[-1]]
        assert all(x == 4 for x in should_be_4), str(should_be_4)

        should_have_same_batch_size = [bottom[1].data.shape[0], bottom[2].data.shape[0], bottom[3].data.shape[0],
                                       bottom[4].data.shape[0]]
        assert all(x == batch_size for x in should_have_same_batch_size), str(should_have_same_batch_size)

        # we assume a certain dimensionality to the bottom blobs that might change,
        # so it will crash here if it ever does:
        dim_of_bottoms = [7 - len(bottom[2].data.shape), len(bottom[3].data.shape), len(bottom[4].data.shape)]
        assert all(x == 3 for x in dim_of_bottoms), str(dim_of_bottoms)

        # if the extra bottom blob is here
        if self._prior_output_mode:
            assert len(bottom[5].data.shape) == 3  # implementation choice, should be reshaped prior to this
            assert bottom[5].data.shape[0] == 2
            assert bottom[5].data.shape[1] == num_priors
            assert bottom[5].data.shape[2] == 4
        # make sure the original gt boxes are supplied
        if self._gt_box_output_on:
            assert bottom[2].data.shape[1] == 2

        number_of_fg_boxes = []
        number_of_bg_boxes_taken = []
        # number_of_easy_bg_taken_debug = []
        # number_of_hard_bg_taken_debug = []

        for batch_index in range(batch_size):
            # build the list of indexes for localization loss
            # this one is excluding boxes that were matched with ignore boxes but including input bg matching
            indexes_of_matched_priors = np.where(bottom[3].data[batch_index, :, :].flatten() >= 0)[0].astype(np.int64)

            if self._easy_bg_max_iou_with_ignore > 0:
                # we find all boxes that are not matched or matched with ignore with small iou
                # (checking any label l.t. 0 and score < threshold)
                indexes_non_matched_priors = np.where(np.bitwise_and(
                    bottom[3].data[batch_index, ...].flatten() < 0,
                    bottom[4].data[batch_index, ...].flatten() < self._easy_bg_max_iou_with_ignore))[0].astype(np.int64)
            else:
                # we find just unmatched boxes
                # (score is "null")
                indexes_non_matched_priors = \
                    np.where(bottom[4].data[batch_index, ...].flatten() < 0)[0].astype(np.int64)

            if DEBUG:
                s_matched = set(indexes_of_matched_priors)
                s_non_matched = set(indexes_non_matched_priors)
                bug = s_matched.intersection(s_non_matched)
                try:
                    assert len(bug) == 0
                except AssertionError:
                    print('Bug - priors are matched against fg and bg??')

            prior_matching_scores = bottom[4].data[batch_index, :, :].flatten()
            # take the scores of which priors that were matched to a non-ignored label
            matched_prior_scores = prior_matching_scores[indexes_of_matched_priors]
            # sort and keep original order because we will use them later

            mask_fg_above_iou_thresh = matched_prior_scores >= self._fg_iou_thresh
            if not self._hw_similarity_prefer_smaller_gt:
                just_fg_boxes = np.where(mask_fg_above_iou_thresh)[0]
            else:
                all_matched_gt_boxes = bottom[2].data[batch_index, 1, indexes_of_matched_priors]
                gt_boxes_area = bboxes_area(all_matched_gt_boxes)
                very_small_box_area = 0.005
                priors_whose_gt_is_small = gt_boxes_area < very_small_box_area
                if DEBUG and np.any(priors_whose_gt_is_small):
                    pass
                just_fg_boxes = np.where(np.bitwise_or(mask_fg_above_iou_thresh, priors_whose_gt_is_small))[0]

            # we want to prevent cases where no boxes are matched and we try to access them:
            if just_fg_boxes.size == 0:
                # no boxes matched :(
                number_of_fg_boxes.append(0)
                indexes_of_good_boxes = np.array([], dtype=np.int64)
            else:
                # convert to original indexing
                fg_boxes_indexes = indexes_of_matched_priors[just_fg_boxes]

                # metrics for similarity threshold
                if not self._hw_similarity_prefer_smaller_gt:
                    hw_similarity_max = np.max(np.abs(bottom[2].data[batch_index, 0, fg_boxes_indexes, 2:4]), axis=1)
                else:
                    hw_params = bottom[2].data[batch_index, 0, fg_boxes_indexes, 2:4]
                    # small boxes which need shrinking get a skew in their similarity test
                    fg_gt_boxes = bottom[2].data[batch_index, 1, fg_boxes_indexes]
                    gt_boxes_area = bboxes_area(fg_gt_boxes)
                    small_box_area = 0.005
                    priors_whose_gt_is_small = gt_boxes_area < small_box_area
                    prior_need_shrink = np.argmin(hw_params, axis=1) == np.argmax(np.abs(hw_params), axis=1)
                    similarity_boost_mask = np.bitwise_and(priors_whose_gt_is_small, prior_need_shrink)
                    fail_threshold = np.bitwise_and(similarity_boost_mask,
                                                    np.max(np.abs(hw_params), axis=1) > self._hw_similarity_thresh)
                    if np.any(fail_threshold):
                        # cheat:
                        hw_params[fail_threshold] *= 0.5
                        # if DEBUG:
                        #     print("found small %d gt whose prior needs to shrink" % hw_params[fail_threshold].size)
                    hw_similarity_max = np.max(np.abs(hw_params), axis=1)

                # use additional threshold on xy positioning
                if self._xy_similarity_thresh > 0:
                    xy_similarity_max = np.max(np.abs(bottom[2].data[batch_index, 0, fg_boxes_indexes, 0:2]), axis=1)
                    not_outliers = np.where(np.bitwise_and(hw_similarity_max <= self._hw_similarity_thresh,
                                                           xy_similarity_max <= self._xy_similarity_thresh))[0]
                else:
                    not_outliers = np.where(hw_similarity_max <= self._hw_similarity_thresh)[0]

                indexes_of_good_boxes = fg_boxes_indexes[not_outliers]
                if indexes_of_good_boxes.size > self._max_num_fg_box_per_image:
                    indexes_of_good_boxes = np.random.choice(indexes_of_good_boxes,
                                                             self._max_num_fg_box_per_image, replace=False)
                number_of_fg_boxes.append(indexes_of_good_boxes.size)
                # make sure no hardbg can mistakenly be selected from the fg boxes
                # convert back to indexes in the "indexes_of_matched_priors"
                indexes_of_matched_priors = np.delete(indexes_of_matched_priors, just_fg_boxes[not_outliers])
                matched_prior_scores = np.delete(matched_prior_scores, just_fg_boxes[not_outliers])

            # these box indexes are for the localization loss
            self._loc_index[batch_index] = indexes_of_good_boxes[:]
            if DEBUG:
                try:
                    assert self._loc_index[batch_index].size == len(set(self._loc_index[batch_index]))
                except AssertionError:
                    print('Bug, duplicate fg indexes found')
                    pass
            # now we use this list to populate the confidence list, which we will add on to later
            self._conf_index[batch_index] = self._loc_index[batch_index][:]

            # find out the requierment based on the bg/fg ratio and boost it if it is small
            how_many_bg_boxes = int(np.floor(number_of_fg_boxes[-1] * float(self._bg_to_fg_box_ratio)))
            if how_many_bg_boxes < self._boost_bg_if_fg_count_low * self._max_conf_out / float(batch_size):
                how_many_bg_boxes = int(self._max_conf_out / float(batch_size)) - number_of_fg_boxes[-1]

            # how many hard bg boxes do we want?
            num_of_wanted_hard_bg = int(np.floor(how_many_bg_boxes * float(min(self._hard_bg_mix, 1))))
            number_of_hard_bg = 0
            hard_bg_indexes = None
            hard_bg_indexes = np.where(matched_prior_scores < self._hard_bg_max_iou)[0]
            if hard_bg_indexes.size > 0:
                number_of_hard_bg = min(hard_bg_indexes.size, num_of_wanted_hard_bg)
                hard_bg_indexes = np.random.choice(hard_bg_indexes, number_of_hard_bg, replace=False)
                # convert to prior indexes
                hard_bg_indexes = indexes_of_matched_priors[hard_bg_indexes]
                # now we add the examples of "hard" bg, to be matched with the bg class
                if hard_bg_indexes.size > 0:
                    self._conf_index[batch_index] = np.hstack((self._conf_index[batch_index], hard_bg_indexes))
                    if DEBUG:
                        bug = set()
                        try:
                            loc_indexes = set(self._loc_index[batch_index])
                            hard_bg_indexes_test = set(hard_bg_indexes)
                            bug = loc_indexes.intersection(hard_bg_indexes_test)
                            assert len(bug) == 0
                        # TODO: if block
                        except AssertionError:
                            print('Bug, some hard bg are also fg {}'.format(sorted(bug)))

            # Now find false positives within the non matched examples
            number_of_easy_bg = min(indexes_non_matched_priors.size, (how_many_bg_boxes - number_of_hard_bg))
            num_wanted_false_positives = int(np.floor(number_of_easy_bg * float(min(self._false_pos_mix, 1))))

            false_positives = np.array([])
            num_false_positives = 0
            if num_wanted_false_positives > 0 or self._ignore_false_pos:
                argmax_of_non_matched = np.argmax(bottom[0].data[batch_index, indexes_non_matched_priors, :], axis=1)
                # those whose argmax should be 0 but are not:
                false_positives = np.where(argmax_of_non_matched != 0)[0]
                # transform to prior indexes:
                false_positives = indexes_non_matched_priors[false_positives]
                if not self._ignore_false_pos:
                    if false_positives.size > num_wanted_false_positives:
                        # need to sort and take the top k
                        if not self._hard_example_mining:
                            # sort by background-ness
                            background_score = bottom[0].data[batch_index, false_positives, 0].flatten()
                            false_pos_indexes_sorted_by_score = np.argsort(background_score)
                        else:
                            false_positives_confidences = bottom[0].data[batch_index, false_positives, :]
                            false_positives_scores = np.apply_along_axis(softmax, 1, false_positives_confidences)
                            false_max_scores = np.max(false_positives_scores[:, 1:], axis=1)
                            false_pos_indexes_sorted_by_score = np.argsort(false_max_scores)[::-1]

                        sorted_candidate_indexes = false_positives[false_pos_indexes_sorted_by_score]

                        if self._pick_hard_false_pos:
                            # take num_wanted_false_positives with lowest probability to be background
                            false_positives = sorted_candidate_indexes[:num_wanted_false_positives]
                        else:
                            # take num_wanted_false_positives with highest probability to be background
                            false_positives = sorted_candidate_indexes[-num_wanted_false_positives:]
                    num_false_positives = false_positives.size
                    self._conf_index[batch_index] = np.hstack((self._conf_index[batch_index], false_positives))
                    if DEBUG:
                        try:
                            assert self._conf_index[batch_index].size == np.unique(self._conf_index[batch_index]).size
                        except AssertionError:
                            print('Bug, fp introduced duplicate indexes')
                            pass
                elif false_positives.size > 0:
                    # assert num_wanted_false_positives == 0, "logic borked"
                    # we just find the false positives with conf over a threshold without inserting them into the list
                    false_positives_confidences = bottom[0].data[batch_index, false_positives, :]
                    false_positives_scores = np.apply_along_axis(softmax, 1, false_positives_confidences)
                    false_max_scores = np.max(false_positives_scores[:, 1:], axis=1)
                    false_positives, = np.where(false_max_scores >= self._ignore_false_pos_conf_thresh)
                # now delete the false positives we chose from the array so that we dont choose them as easy bg as well
                if false_positives.size > 0:
                    indexes_non_matched_priors = np.delete(indexes_non_matched_priors, false_positives)

            # update projection for easy bg:
            number_of_easy_bg = min(indexes_non_matched_priors.size,
                                    (how_many_bg_boxes - number_of_hard_bg - num_false_positives))
            # finish up with some general (maybe random) boxes for the easy match
            real_easy_bg_indexes = None
            if number_of_easy_bg > 0:
                if (indexes_non_matched_priors.size - number_of_easy_bg) <= 3:
                    real_easy_bg_indexes = indexes_non_matched_priors[:number_of_easy_bg]
                else:
                    if not self._prefer_largest:
                        # easy bg: use random, but take from list generated above
                        real_easy_bg_indexes = np.random.choice(indexes_non_matched_priors,
                                                                number_of_easy_bg, replace=False)
                    else:
                        # easy bg: take the boxes according to sizes probability p
                        p_wanted = [0.4, 0.3, 0.3]
                        # fake distributions of sizes
                        p_priors = [0.5, 0.45, 0.05]
                        med_boxes_num = int(number_of_easy_bg * p_wanted[1])
                        med_boxes_val = int(indexes_non_matched_priors.size * p_priors[1]) + 1
                        large_boxes_num = int(number_of_easy_bg * p_wanted[2])
                        large_boxes_val = int(indexes_non_matched_priors.size * p_priors[2]) + 1
                        large_index = indexes_non_matched_priors.size - max(large_boxes_num, large_boxes_val)
                        # the rest are the small
                        small_boxes_num = number_of_easy_bg - med_boxes_num - large_boxes_num
                        small_index = max(small_boxes_num, large_index - med_boxes_val)

                        if DEBUG:
                            try:
                                assert np.arange(small_index, large_index).size >= med_boxes_num
                            except AssertionError:
                                print('bug in easy bg box partitioning')

                        # choose from small boxes
                        small_bg_box_indexes = [] if small_boxes_num < 1 else \
                            np.random.choice(small_index, small_boxes_num,
                                             replace=False)
                        # choose from medium boxes
                        med_bg_box_indexes = [] if med_boxes_num < 1 else \
                            np.random.choice(np.arange(small_index, large_index), med_boxes_num,
                                             replace=False)
                        # choose from large boxes
                        large_bg_box_indexes = [] if large_boxes_num < 1 else\
                            np.random.choice(np.arange(large_index, indexes_non_matched_priors.size), large_boxes_num,
                                             replace=False)

                        indexes_of_bg = np.hstack((small_bg_box_indexes, med_bg_box_indexes, large_bg_box_indexes))\
                                            .astype(np.int).tolist()

                        real_easy_bg_indexes = [] if not indexes_of_bg else indexes_non_matched_priors[indexes_of_bg]
                        if DEBUG:
                            # if large_bg_box_indexes.size == large_boxes_num:
                            #     print('maxed out available large background for easy')
                            if number_of_easy_bg > real_easy_bg_indexes.size:
                                print('Bug in easy bg, too many bg!')
                if DEBUG:
                    try:
                        assert real_easy_bg_indexes.size == np.unique(real_easy_bg_indexes).size
                    except AssertionError:
                        print('Bug, duplicate indexes found')
                        pass
                self._conf_index[batch_index] = np.hstack((self._conf_index[batch_index], real_easy_bg_indexes))
                number_of_easy_bg = real_easy_bg_indexes.size + num_false_positives
            number_of_bg_boxes_taken.append(number_of_easy_bg + number_of_hard_bg)
            if DEBUG and number_of_bg_boxes_taken[-1] > self._max_conf_out / batch_size:
                print('Bug, too many bg taken')
                pass

            if self._top_9_debug_priors_out:
                lbl_ez_bg = 10009
                lbl_hard_bg = 10012
                lbl_fp_bg = 10101
                lbl_fg = 11000
                # priors selected for fg
                self._debug_indexes[batch_index] = self._loc_index[batch_index][:]
                self._debug_labels[batch_index] = [lbl_fg] * len(self._loc_index[batch_index])
                self._debug_scores[batch_index] = prior_matching_scores[self._debug_indexes[batch_index]]
                i_think_n_good_boxes = len(self._debug_indexes[batch_index])
                assert i_think_n_good_boxes == len(self._loc_index[batch_index])
                # priors selected for hard bg
                if hard_bg_indexes is not None:
                    self._debug_indexes[batch_index] = \
                        np.hstack((self._debug_indexes[batch_index], hard_bg_indexes))
                    self._debug_labels[batch_index] = \
                        np.hstack((self._debug_labels[batch_index], [lbl_hard_bg] * len(hard_bg_indexes)))
                    self._debug_scores[batch_index] = \
                        np.hstack((self._debug_scores[batch_index], prior_matching_scores[hard_bg_indexes]))
                    i_think_n_hard_bg = len(self._debug_indexes[batch_index])
                    if i_think_n_hard_bg > 0:
                        i_think_n_hard_bg -= i_think_n_good_boxes
                    assert i_think_n_hard_bg == number_of_hard_bg
                # priors selected for fp bg
                if num_wanted_false_positives > 0:
                    self._debug_indexes[batch_index] = \
                        np.hstack((self._debug_indexes[batch_index], false_positives))
                    self._debug_labels[batch_index] = \
                        np.hstack((self._debug_labels[batch_index], [lbl_fp_bg] * len(false_positives)))
                    self._debug_scores[batch_index] = \
                        np.hstack((self._debug_scores[batch_index], prior_matching_scores[false_positives]))
                # priors selected for easy bg
                if number_of_easy_bg > 0:
                    self._debug_indexes[batch_index] = \
                        np.hstack((self._debug_indexes[batch_index], real_easy_bg_indexes))
                    self._debug_labels[batch_index] = \
                        np.hstack((self._debug_labels[batch_index], [lbl_ez_bg] * len(real_easy_bg_indexes)))
                    self._debug_scores[batch_index] = \
                        np.hstack(
                        (self._debug_scores[batch_index], prior_matching_scores[real_easy_bg_indexes]))

        # make sure tops are adequately sized
        num_of_loc_boxes = sum(x.size for x in self._loc_index)
        num_of_conf_boxes = sum(x.size for x in self._conf_index)

        good_box_lists = self._loc_index
        num_boxes = num_of_loc_boxes

        # new: now with padding using the last index
        # check to see if any of the outputs is larger than the designated max size
        num_loc_pad = self._max_loc_out - num_boxes  # supports debug mode like this. dont change to num_of_loc_boxes
        num_conf_pad = self._max_conf_out - num_of_conf_boxes

        if num_loc_pad < 0 or num_conf_pad < 0:
            raise OverflowError("somehow number of selected good boxes is higher than the maximum blob size")

        self._reweight_loc_loss_because_padding = self._max_loc_out / float(max(1, num_boxes))
        self._reweight_conf_loss_because_padding = self._max_conf_out / float(max(1, num_of_conf_boxes))

        # write data:
        # first lets get the priors out of the way since its a special case
        if self._prior_output_mode:
            if num_loc_pad == 0:
                # stack the original priors and variances (needed for decoding the good-box input)
                top[5].data[0:num_boxes, :] = np.array(
                    (np.vstack(bottom[5].data[0, indexes, :] for indexes in good_box_lists if len(indexes) > 0),
                     np.vstack(bottom[5].data[1, indexes, :] for indexes in good_box_lists if len(indexes) > 0)),
                    order='C')
            else:
                prior_padding = np.tile(np.array(bottom[5].data[0, 0, :], copy=True, order='C'), [num_loc_pad, 1])
                if num_boxes > 0:
                    prior_positions = np.vstack(
                        (np.vstack(bottom[5].data[0, indexes, :] for indexes in good_box_lists if len(indexes) > 0),
                         prior_padding))
                else:
                    prior_positions = prior_padding

                variance_padding = np.tile(np.array(bottom[5].data[1, -1, :], copy=True, order='C'), [num_loc_pad, 1])
                if num_boxes > 0:
                    variances = np.vstack(
                        (np.vstack(bottom[5].data[1, indexes, :] for indexes in good_box_lists if len(indexes) > 0),
                         variance_padding))
                else:
                    variances = variance_padding

                top[5].data[...] = np.array((prior_positions, variances), order='C')

        # good boxes:
        if num_boxes:
            top[2].data[0:num_boxes, :] = np.vstack(bottom[1].data[n, indexes, :]
                                                    for (n, indexes) in enumerate(good_box_lists) if len(indexes) > 0)
            top[3].data[0:num_boxes, :] = np.vstack(bottom[2].data[n, 0, indexes, :]
                                                    for (n, indexes) in enumerate(good_box_lists) if len(indexes) > 0)

            if self._index_blob_on:
                # todo fill with number per list
                # todo (2) understand what I wanted here^
                top[4].data[0, 0, 0, 0:num_boxes] = \
                    np.squeeze(np.vstack(np.full((len(indexes), 1), n, dtype=top[3].data.dtype)
                                         for (n, indexes) in enumerate(good_box_lists) if len(indexes) > 0))

            if self._gt_box_output_on:
                top[6].data[0:num_boxes, :] = \
                    np.vstack(bottom[2].data[n, 1, indexes, :]
                              for (n, indexes) in enumerate(good_box_lists) if len(indexes) > 0)

            if self._loc_conf_output_on:
                good_box_conf_output =\
                    np.vstack(bottom[0].data[n, indexes, :]
                              for (n, indexes) in enumerate(good_box_lists) if len(indexes) > 0)
                top[7].data[0:num_boxes, :] = np.apply_along_axis(softmax, 1, good_box_conf_output)

        if num_of_conf_boxes:
            top[0].data[0, 0, 0:num_of_conf_boxes, :] = \
                np.vstack(bottom[0].data[n, indexes, :]
                          for (n, indexes) in enumerate(self._conf_index) if len(indexes) > 0)
            int_ptr = 0
            bg_class_np = np.array(self._background_class_index, dtype=bottom[0].data.dtype)
            for batch_index in range(batch_size):
                if len(self._conf_index[batch_index]) > 0:
                    if DEBUG:
                        assert self._conf_index[batch_index].size == \
                               number_of_fg_boxes[batch_index] + number_of_bg_boxes_taken[batch_index], "Bug"


                    bg_box_start_index = int_ptr + number_of_fg_boxes[batch_index]
                    bg_box_end_index = int_ptr + number_of_fg_boxes[batch_index] + number_of_bg_boxes_taken[batch_index]
                    # matched labels
                    fg_boxes_indexes = self._conf_index[batch_index][:number_of_fg_boxes[batch_index]]
                    top[1].data[0, 0, 0, int_ptr:bg_box_start_index] =\
                        np.squeeze(bottom[3].data[batch_index, :, fg_boxes_indexes])
                    # the label ground truth is dealt with a little differently
                    # because we need to force match with bg:
                    top[1].data[0, 0, 0, bg_box_start_index:bg_box_end_index] = bg_class_np
                    # move on to next image
                    int_ptr += self._conf_index[batch_index].size

        # finally all the padding
        index_pad = np.array([-1], dtype=bottom[0].data.dtype)
        bg_class_np_pad = np.array(self._background_class_index, dtype=bottom[0].data.dtype)
        padding_loss_row = np.ones_like(bottom[0].data[0, 0, :], order='C')
        # enforce bg class to win the softmax on padded rows so loss contribution will be zero
        padding_loss_row[0] = 200.0

        if num_conf_pad > 0:
            # print("conf pad %d" % num_conf_pad)
            top[0].data[0, 0, num_of_conf_boxes:num_of_conf_boxes + num_conf_pad, :] = padding_loss_row
            top[1].data[0, 0, 0, num_of_conf_boxes:num_of_conf_boxes + num_conf_pad] = bg_class_np_pad

        if num_loc_pad > 0:
            box_padding = np.zeros_like(bottom[1].data[0, 0, :], order='C')
            # print("loc pad %d" % num_loc_pad)
            top[2].data[num_boxes:num_boxes + num_loc_pad, :] = box_padding
            top[3].data[num_boxes:num_boxes + num_loc_pad, :] = box_padding
            if self._index_blob_on:
                # add padding with bcast:
                top[4].data[0, 0, 0, num_boxes:num_boxes + num_loc_pad] = index_pad
            if self._gt_box_output_on:
                top[6].data[num_boxes:num_boxes + num_loc_pad, :] = box_padding
            if self._loc_conf_output_on:
                top[7].data[num_boxes:num_boxes + num_loc_pad, :] = padding_loss_row

        if self._top_9_debug_priors_out:
            if num_of_conf_boxes > 0:
                debug_index = np.atleast_2d(np.squeeze(np.vstack(
                    [np.full((len(idx_list), 1), batch_index)
                     for (batch_index, idx_list) in enumerate(self._debug_indexes) if len(idx_list) > 0]))).transpose()
                debug_rois = np.vstack([bottom[5].data[0, idx_list, :] for idx_list in self._debug_indexes])
                debug_labels = np.atleast_2d(np.hstack([lbl_list for lbl_list in self._debug_labels])).transpose()
                debug_scores = np.atleast_2d(np.hstack([scr_list for scr_list in self._debug_scores])).transpose()
                num_report_debug = debug_scores.shape[0]
                shape_debug_boxes = [num_report_debug, 7]
                top[8].reshape(*shape_debug_boxes)
                top[8].data[...] = np.hstack((debug_index, debug_rois, debug_labels, debug_scores)).astype(
                        bottom[0].data.dtype)
            else:
                shape_debug_boxes = [1, 7]
                top[8].reshape(*shape_debug_boxes)
                top[8].data[...] = np.array([0, 0, 0, 0, 0, 0, 0]).astype(bottom[0].data.dtype)

        # print 'number_of_fg_boxes', number_of_fg_boxes
        # print 'number_of_bg_boxes_taken',number_of_bg_boxes_taken
        # print 'number_of_hard_bg_taken_debug ', number_of_hard_bg_taken_debug
        # print 'number_of_easy_bg_taken_debug ', number_of_easy_bg_taken_debug

        # statistics comes last
        if self._report_prior_match:
            i = self._location_of_stat_blob
            if num_of_conf_boxes == 0:
                print('how')
                i_am_ignore = -1 * np.ones([1, 2], dtype=top[i].data.dtype)
                top[i].reshape(*[1, 2])
                top[i].data[...] = i_am_ignore
                return

            labels = [[] for _ in range(batch_size)]
            for batch_index in range(batch_size):
                if len(self._conf_index[batch_index]) > 0:
                    # some safety
                    assert self._conf_index[batch_index].size == number_of_fg_boxes[batch_index] + \
                           number_of_bg_boxes_taken[batch_index], "something went wrong with indexing"
                    # background for everything
                    num_conf = self._conf_index[batch_index].size
                    labels[batch_index] = np.zeros(num_conf, dtype=top[i].data.dtype, order='C')
                    # get labels
                    num_labels = self._conf_index[batch_index][0:number_of_fg_boxes[batch_index]]
                    labels[batch_index][0:number_of_fg_boxes[batch_index]] =\
                        np.atleast_1d(np.squeeze(bottom[3].data[batch_index, :, num_labels]))

            top[i].reshape(*[num_of_conf_boxes, 2])
            top[i].data[...] =\
                np.vstack([np.array(list(zip(self._conf_index[n], labels[n])), dtype=top[i].data.dtype, order='C')
                           for n in range(batch_size) if len(self._conf_index[n]) > 0])

    def backward(self, top, propagate_down, bottom):

        # set appropriate bottom diffs to zero (what about propagate down?)
        i_am_zero = np.zeros(1, dtype=bottom[0].diff.dtype)
        bottom[0].diff[...] = i_am_zero
        bottom[1].diff[...] = i_am_zero

        # disassemble the incoming diffs to their respective images and propagate down
        int_ptr = 0
        for n in range(len(self._conf_index)):
            if len(self._conf_index[n]):
                next_size = self._conf_index[n].size
                assert next_size > 0, "Bug"
                # The average loss is calculated based on _max_conf_out,
                # while the actual number of boxes is lower (due to padding).
                # We reweigh the gradient to reflect the fact that padding gradients are zero
                bottom[0].diff[n, self._conf_index[n], :] = \
                    top[0].diff[0, 0, int_ptr:int_ptr + next_size, :] * \
                    self._reweight_conf_loss_because_padding
                int_ptr += next_size

        int_ptr = 0
        for n in range(len(self._loc_index)):
            if len(self._loc_index[n]):
                next_size = self._loc_index[n].size
                assert next_size > 0
                # The average loss is calculated based on _max_loc_out,
                # while the actual number of boxes is lower (due to padding).
                # We reweigh the gradient to reflect the fact that padding gradients are zero
                bottom[1].diff[n, self._loc_index[n], :] = \
                    top[2].diff[int_ptr:int_ptr + next_size, :] * \
                    self._reweight_loc_loss_because_padding
                int_ptr += next_size
