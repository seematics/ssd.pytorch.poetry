import torch.nn as nn
from torchvision.models.resnet import resnet18, resnet34, resnet50, resnet101, resnet152

from layers.functions.prior_box import NEW_ASPECT_RATIO_KEY, PriorBoxBase
from models.ssd import SKIP, CODE_SIZE, SSD, calculate_priors_per_scale_for_v1_design, add_extras, add_extras_512

RES_EXTRAS = {
    '300': [256, 'S', 512, 128, 'S', 256, 128, 'S', 256],
    '512': [256, 'S', 512, 128, 'S', 256, 128, 'S', 256, 128, 'S', 256],
}


def multibox_resnet(features, extra_layers, priors_per_scale, num_classes, features_source):
    loc_layers = []
    conf_layers = []
    out_channel_list = [features[ind][-1].conv3.out_channels for ind in features_source]
    out_channel_list += [l.out_channels for l in extra_layers[1::2]]
    assert len(priors_per_scale) == len(out_channel_list), "priors_per_scale config error"
    for n_priors, FE_out_chan in zip(priors_per_scale, out_channel_list):
        if not n_priors:
            loc_layers += [SKIP]
            conf_layers += [SKIP]
            continue
        loc_layers += [nn.Conv2d(FE_out_chan, n_priors * CODE_SIZE, kernel_size=3, padding=1)]
        conf_layers += [nn.Conv2d(FE_out_chan, n_priors * num_classes, kernel_size=3, padding=1)]
    return features, extra_layers, (loc_layers, conf_layers)


def build_ssd_with_resnet(cfg, phase, variant=300, size=(300, 300), num_classes=21, extras_in_channels=2048,
                          resnet_type='resnet50', dropout=0.1, logger=None):
    if phase != "test" and phase != "train":
        print("ERROR: Phase: " + phase + " not recognized")
        return

    if resnet_type == 'resnet50':
        network = resnet50()
    elif resnet_type == 'resnet101':
        network = resnet101()
    else:
        raise ValueError("resnet type: " + resnet_type + " not recognized. We support only 'resnet50' and 'resnet101'")

    features = [x for x in network.children()][:-2]
    features_source = [5, 6, -1]
    conv4_3 = (6, 7)
    l2_n_channels = (512, 1024)

    is_v1 = PriorBoxBase.detect_prior_v1(cfg=cfg)
    if is_v1:
        adaptive = NEW_ASPECT_RATIO_KEY in cfg.keys()
        cfg, priors_per_scale = calculate_priors_per_scale_for_v1_design(cfg, backwards_compatible=(not adaptive))
    else:
        prior_design = cfg['prior_design']
        priors_per_scale = PriorBoxBase.calculate_priors_per_scale(prior_design=prior_design)

    add_extras_func = {300: add_extras, 512: add_extras_512}[variant]
    base_, extras_, head_ = multibox_resnet(features=features,
                                            extra_layers=add_extras_func(RES_EXTRAS[str(variant)], extras_in_channels),
                                            priors_per_scale=priors_per_scale, num_classes=num_classes,
                                            features_source=features_source)
    return SSD(phase, size, base_, extras_, head_, num_classes=num_classes, cfg=cfg, conv4_3=conv4_3, dropout=dropout,
               l2_n_channels=l2_n_channels, logger=logger)
