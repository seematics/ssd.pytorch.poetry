import os
from collections import namedtuple

import torch
from allegroai.debugging import Timer
from torch import nn as nn
from torch.nn import functional as F

from common.loading import ModuleWithForgivingLoad
from layers import L2Norm, PriorBox, Detect
from layers.functions.prior_box import NEW_ASPECT_RATIO_KEY, AnchorProto

DetectInput = namedtuple('DetectInput', 'loc_data conf_data prior_data')
SKIP = object()  # filler
CODE_SIZE = 4  # number of coordinates used to describe bounding box
EXTRAS = {
    '300': [256, 'S', 512, 128, 'S', 256, 128, 'S', 256, 128, 'S', 256],
    '512': [256, 'S', 512, 128, 'S', 256, 128, 'S', 256, 128, 'S', 256, 128, 256],
}
MBOX = {'deprecated'}


class SSD(ModuleWithForgivingLoad):
    """Single Shot Multibox Architecture
    The network is composed of a base VGG network followed by the
    added multibox_vgg conv layers.  Each multibox_vgg layer branches into
        1) conv2d for class conf scores
        2) conv2d for localization predictions
        3) associated priorbox layer to produce default bounding
           boxes specific to the layer's feature map size.
    See: https://arxiv.org/pdf/1512.02325.pdf for more details.

    Args:
        phase: (string) Can be "test" or "train"
        size: input image size
        base: VGG16 layers for input, size of either 300 or 500
        extras: extra layers that feed to multibox_vgg loc and conf layers
        head: "multibox_vgg head" consists of loc and conf conv layers
    """

    def __init__(self, phase, size, base, extras, head, num_classes, cfg, conv4_3, dropout=0.1,
                 l2_n_channels=512, logger=None):
        super(SSD, self).__init__(logger=logger)
        self.phase = phase
        self.num_classes = num_classes
        self.cfg = cfg
        self.size = size if isinstance(size, tuple) else (size, size)
        self.conv4_3 = list(conv4_3) if not isinstance(conv4_3, int) else [conv4_3]
        self.dropout_val = dropout

        # SSD network
        self.features = nn.ModuleList(base)
        # Layer learns to scale the l2 normalized features from conv4_3
        self.l2_n_channels = list(l2_n_channels) if not isinstance(l2_n_channels, int) else [l2_n_channels]
        self.L2Norm = nn.ModuleList()
        for n_channels in self.l2_n_channels:
            self.L2Norm.append(L2Norm(n_channels, 20))
        # self.L2Norm = L2Norm(l2_n_channels, 20)
        self.dropout = nn.Dropout2d(p=self.dropout_val, inplace=False)
        self.extras = nn.ModuleList(extras)

        self.loc = nn.ModuleList([l for l in head[0] if l is not SKIP])
        self.conf = nn.ModuleList([l for l in head[1] if l is not SKIP])

        self._source_switch = [t is not SKIP for t in head[0]]

        # create priors:
        # change cfg so that feature_maps hold the actual size of the feature maps
        self.priors = None
        self.priors_info = None
        self._verify_priors_match_output = False

        # post_processing
        self.softmax = nn.Softmax(dim=-1)
        self._conf_thresh = cfg.get('conf_thresh', 0.01)
        self.detect = Detect(num_classes=num_classes, bkg_label=cfg.get('background_label', 0),
                             top_k=cfg.get('top_k', 200),
                             conf_thresh=self._conf_thresh,
                             nms_thresh=cfg.get('nms_thresh', 0.45),
                             cfg=cfg)

        self._timers_on = cfg.get('benchmark_timers', False)
        if self._timers_on:
            self._cuda_need_sync = True if self._timers_on == 'cuda' else False
            self._fw_timer = Timer()
            self._detect_timer = Timer()

    @property
    def default_conf_thresh(self):
        return self._conf_thresh

    @default_conf_thresh.setter
    def default_conf_thresh(self, new_thresh):
        self._conf_thresh = new_thresh

    def generate_priors(self, priorbox_cls=PriorBox, feature_sizes_wh=None):
        with torch.no_grad():
            priors, info = priorbox_cls(self.cfg, override_feature_sizes_wh=feature_sizes_wh, logger=self._logger).forward()
        self.priors = priors
        self.priors_info = info

    def get_multiscale_feature_sizes(self, input_image, bypass_source_switch=False):
        return [AnchorProto(w=s.shape[-1], h=s.shape[-2]) for s in self.process_input_to_multiscale_features(
            x=input_image, bypass_source_switch=bypass_source_switch)]

    # timers
    def reset_timers(self):
        if self._timers_on:
            self._fw_timer.reset()
            self._detect_timer.reset()

    def get_average_times(self):
        if not self._timers_on:
            return dict()
        return dict(fw_time=self._fw_timer.average_time, detect_time=self._detect_timer.average_time)

    def forward(self, x, conf_thresh=None):
        """Applies network layers and ops on input image(s) x.

        Args:
            x: input image or batch of images. Shape: [batch,3,300,300].
            conf_thresh (for eval mode) change online the detection conf threshold


        Return:
            Depending on phase:
            test_orig:
                Variable(tensor) of output class label predictions,
                confidence score, and corresponding location predictions for
                each object detected. Shape: [batch,topk,7]

            train_orig:
                list of concat outputs from:
                    1: confidence layers, Shape: [batch*num_priors,num_classes]
                    2: localization layers, Shape: [batch,num_priors*4]
                    3: priorbox layers, Shape: [2,num_priors*4]
        """
        if self.priors is None:
            raise RuntimeError('net cannot be used without calling generate_priors first')

        if self._timers_on:
            if self._cuda_need_sync:
                torch.cuda.synchronize()
            self._fw_timer.tic()

        sources = self.process_input_to_multiscale_features(x)
        # apply multibox head to source layers
        conf, loc = self.multibox_heads(sources)

        if self._timers_on:
            if self._cuda_need_sync:
                torch.cuda.synchronize()
            self._fw_timer.toc()

        if not self.training:
            self.detect.conf_thresh = conf_thresh if conf_thresh else self.default_conf_thresh
            detect_input = DetectInput(loc_data=loc.view((loc.size(0), -1, 4)),
                                       conf_data=self.softmax(conf.view((conf.size(0), -1, self.num_classes))),
                                       prior_data=self.priors)
            if self._timers_on:
                if self._cuda_need_sync:
                    torch.cuda.synchronize()
                self._detect_timer.tic()

            output = self.detect(*detect_input).clamp(0., 1.)

            if self._timers_on:
                if self._cuda_need_sync:
                    torch.cuda.synchronize()
                self._detect_timer.toc()
        else:
            output = (
                loc.view((loc.size(0), -1, 4)),
                conf.view((conf.size(0), -1, self.num_classes)),
                self.priors.view((1, -1, 4))  # for concating
            )
        return output

    def multibox_heads(self, sources):
        loc = list()
        conf = list()
        for (x, l, c) in zip(sources, self.loc, self.conf):
            loc.append(l(x).permute(0, 2, 3, 1).contiguous())
            conf.append(c(x).permute(0, 2, 3, 1).contiguous())
        loc = torch.cat([o.view(o.size(0), -1) for o in loc], 1)
        conf = torch.cat([o.view(o.size(0), -1) for o in conf], 1)

        # sanity test run once - test that number of priors matches loc and conf
        if self._verify_priors_match_output:
            assert int(loc.shape[-1])/4 == int(self.priors.shape[0]), "Bug!"
            assert int(conf.shape[-1])/self.num_classes == int(self.priors.shape[0]), "Bug!"
            self._verify_priors_match_output = False

        return conf, loc

    def process_input_to_multiscale_features(self, x, bypass_source_switch=False):
        multiscale_feature_v = list()
        extraction_layer_index = 0
        # supporting multiple interim layers as ssd input
        conv4_3 = self.conv4_3[extraction_layer_index]
        for k in range(len(self.features)):
            x = self.features[k](x)
            if k == conv4_3 - 1:
                if self._source_switch[extraction_layer_index] or bypass_source_switch:
                    s = self.L2Norm[extraction_layer_index](x)
                    multiscale_feature_v.append(s)
                extraction_layer_index += 1
                conv4_3 = self.conv4_3[extraction_layer_index] if extraction_layer_index < len(self.conv4_3) else 0
        layer_index_after_conv4_3 = extraction_layer_index
        if self._source_switch[layer_index_after_conv4_3] or bypass_source_switch:
            multiscale_feature_v.append(x)
        # apply dropout
        if self.training and self.dropout_val > 0:
            x = self.dropout(x)
        # apply extra layers and cache source layer outputs
        # NEW: only if source switch is active
        for k, v in enumerate(self.extras):
            x = F.relu(v(x), inplace=True)
            if k % 2 == 1 and (bypass_source_switch or
                               self._source_switch[layer_index_after_conv4_3 + 1 + (k - 1) // 2]):
                multiscale_feature_v.append(x)
        return multiscale_feature_v

    def load_weights(self, base_file):
        print('load_weights is not supported for safe loading. use forgiving_load')
        other, ext = os.path.splitext(base_file)
        if ext == '.pkl' or '.pth':
            print('Loading weights into state dict...')
            self.load_state_dict(torch.load(base_file,
                                 map_location=lambda storage, loc: storage))
            print('Finished loading weights')
        else:
            print('Sorry only .pth and .pkl files supported.')


def calculate_priors_per_scale_for_v1_design(cfg, backwards_compatible=False):
    try:
        aspect_ratio_key = NEW_ASPECT_RATIO_KEY
        aspect_ratio_lists = cfg[aspect_ratio_key]
        old_style = False
    except KeyError:
        if not backwards_compatible:
            raise KeyError("Expecting '{}' key in the configuration for multibox_vgg".format(aspect_ratio_key))
        else:
            old_style = True
            aspect_ratio_key = 'aspect_ratios'
            try:
                aspect_ratio_lists = cfg[aspect_ratio_key]
            except KeyError:
                raise KeyError("Using backwards compatible config, "
                               "'{}' key is missing the configuration for multibox_vgg".format(aspect_ratio_key))

    if not all([isinstance(t, list) for t in aspect_ratio_lists]):
        raise ValueError("In network design: '{}' is not a list of lists".format(aspect_ratio_key))

    rev_aspect_ratio_list = [[ar for ar in ar_list if ar != 0] for ar_list in aspect_ratio_lists]
    priors_per_scale = [len(l)*2 + 2 for l in rev_aspect_ratio_list] if old_style else\
        [len(l) for l in rev_aspect_ratio_list]
    # make sure to delete the resolutions you do not need:
    update_keys = [k for k in ['min_sizes', 'max_sizes'] if k in cfg]
    for size_key in update_keys:
        cfg[size_key] = [size for n, size in zip(priors_per_scale, cfg[size_key]) if n > 0]
    cfg[aspect_ratio_key] = [l for n, l in zip(priors_per_scale, cfg[aspect_ratio_key]) if n > 0]
    return cfg, priors_per_scale


def add_extras(cfg, i):
    # Extra layers added to VGG for feature scaling
    layers = []
    in_channels = i
    flag = False
    for k, v in enumerate(cfg[:-2]):
        if in_channels != 'S':
            if v == 'S':
                layers += [nn.Conv2d(in_channels, cfg[k + 1], kernel_size=(1, 3)[flag], stride=2, padding=1)]
            else:
                layers += [nn.Conv2d(in_channels, v, kernel_size=(1, 3)[flag])]
            flag = not flag
        in_channels = v
    layers += [nn.Conv2d(in_channels, cfg[-1], kernel_size=3)]
    return layers


def add_extras_512(cfg, i, hmap_channels=0):
    # Extra layers added to VGG for feature scaling
    layers = []
    add_hmap_chans = 1
    in_channels = i
    flag = False
    for k, v in enumerate(cfg[:-1]):
        if in_channels != 'S':
            if v == 'S':
                layers += [nn.Conv2d(in_channels, cfg[k + 1], kernel_size=(1, 3)[flag], stride=2, padding=1)]
            else:
                layers += [nn.Conv2d(in_channels + add_hmap_chans*hmap_channels, v, kernel_size=(1, 3)[flag])]
            flag = not flag
            add_hmap_chans = 0
        else:
            add_hmap_chans = 1
        in_channels = v
    if cfg[-2] != 'S':
        layers += [nn.Conv2d(cfg[-2], cfg[-1], kernel_size=4, padding=1)]
    return layers
