
import torch.nn as nn
import numpy as np
from torchvision.models.mobilenet import mobilenet_v2, ConvBNReLU, InvertedResidual

from layers.functions.prior_box import NEW_ASPECT_RATIO_KEY, PriorBoxBase
from models.ssd import SKIP, CODE_SIZE, SSD, calculate_priors_per_scale_for_v1_design


class ConvReLU(nn.Sequential):
    def __init__(self, in_planes, out_planes, kernel_size=3, stride=1, groups=1):
        padding = (kernel_size - 1) // 2
        super(ConvReLU, self).__init__(
            nn.Conv2d(in_planes, out_planes, kernel_size, stride, padding, groups=groups, bias=False),
            nn.ReLU6(inplace=True)
        )


class InvertedResidualNOBN(nn.Module):
    def __init__(self, inp, oup, stride, expand_ratio, no_bn_for_bones=False):
        super().__init__()
        unit = ConvReLU if no_bn_for_bones else ConvBNReLU
        self.stride = stride
        assert stride in [1, 2]

        hidden_dim = int(round(inp * expand_ratio))
        self.use_res_connect = self.stride == 1 and inp == oup

        layers = []
        if expand_ratio != 1:
            # pw
            layers.append(unit(inp, hidden_dim, kernel_size=1))
        layers.extend([
            # dw
            unit(hidden_dim, hidden_dim, stride=stride, groups=hidden_dim),
            # pw-linear
            nn.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False),
        ])

        self.conv = nn.Sequential(*layers)

    def forward(self, x):
        if self.use_res_connect:
            return x + self.conv(x)
        else:
            return self.conv(x)


class MobilenetSSD(SSD):
    def __init__(self, phase, size, base, extras,
                 head, num_classes, cfg, conv4_3, dropout=0.1, logger=None):
        l2_n_channels = []
        super().__init__(phase, size, base, extras, head, num_classes, cfg, conv4_3, dropout, l2_n_channels, logger)

    def process_input_to_multiscale_features(self, x, bypass_source_switch=False):
        multiscale_feature_v = list()
        extraction_layer_index = 0
        # supporting multiple interim layers as ssd input
        conv4_3 = self.conv4_3[extraction_layer_index]
        # self._logger.console('input: x shape now :{}'.format(x.shape))
        for k in range(len(self.features)):
            if k == conv4_3 and (self._source_switch[extraction_layer_index] or bypass_source_switch):
                parts = list(self.features[k].conv.children())
                x = parts[0](x)
                x = parts[1](x)
                multiscale_feature_v.append(x)
                x = parts[2](x)
                x = parts[3](x)
                extraction_layer_index += 1
                conv4_3 = self.conv4_3[extraction_layer_index] if extraction_layer_index < len(self.conv4_3) else 0
            else:
                x = self.features[k](x)
                # self._logger.console('{}: x shape now :{}'.format(k, x.shape))
        layer_index_after_conv4_3 = extraction_layer_index
        if self._source_switch[layer_index_after_conv4_3] or bypass_source_switch:
            multiscale_feature_v.append(x)
        # apply dropout
        if self.training and self.dropout_val > 0:
            x = self.dropout(x)
        # apply extra layers and cache source layer outputs
        # NEW: only if source switch is active
        for k, v in enumerate(self.extras):
            x = v(x)
            # self._logger.console('EXTRAS {}: x shape now :{}'.format(k, x.shape))
            if bypass_source_switch or self._source_switch[layer_index_after_conv4_3 + 1 + (k - 1) // 2]:
                multiscale_feature_v.append(x)
        return multiscale_feature_v


def multibox_detection_branches_mobilenet(out_channel_list, priors_per_scale, num_classes):
    loc_layers = []
    conf_layers = []
    assert len(priors_per_scale) == len(out_channel_list), "priors_per_scale config error"
    for n_priors, FE_out_chan in zip(priors_per_scale, out_channel_list):
        if not n_priors:
            loc_layers += [SKIP]
            conf_layers += [SKIP]
            continue
        # for ssdlite exchange these with separableconv (maybe not the last layer?)
        loc_layers += [nn.Conv2d(FE_out_chan, n_priors * CODE_SIZE, kernel_size=3, padding=1)]
        conf_layers += [nn.Conv2d(FE_out_chan, n_priors * num_classes, kernel_size=3, padding=1)]
    return (loc_layers, conf_layers)


mobilenet_param_defaults = dict(width_mult=1.0)


def build_ssd_with_mobilenetv2(cfg, phase, variant=300, size=(300, 300), num_classes=21, dropout=0.1,
                               mobilenet_params=None, logger=None):
    if phase != "test" and phase != "train":
        print("ERROR: Phase: " + phase + " not recognized")
        return

    mobilenet_params = mobilenet_params if mobilenet_params is not None else mobilenet_param_defaults
    mobilenet_params.update(dict(num_classes=num_classes))

    np.testing.assert_allclose(mobilenet_params['width_mult'], 1.0)

    network = mobilenet_v2(pretrained=False, **mobilenet_params)
    last_fe_channel = network.last_channel
    features = network.features

    # TODO:
    conv4_3 = (6, 13)
    # l2_n_channels = (512, 1024)

    # add_extras_func = {300: add_extras, 512: add_extras_512}[variant]
    # extras = add_extras_func(RES_EXTRAS, i=last_fe_channel)
    extras = [
        InvertedResidual(last_fe_channel, 512, stride=2, expand_ratio=0.2),
        InvertedResidual(512, 256, stride=2, expand_ratio=0.25),
        InvertedResidual(256, 256, stride=2, expand_ratio=0.5),
    ]
    # if variant == 512:
    #     extras.append(
    #         InvertedResidual(256, 256, stride=2, expand_ratio=0.5),
    #     )
    extras.append(InvertedResidualNOBN(256, 64, stride=2, expand_ratio=0.25, no_bn_for_bones=True))

    # build out_channel_list
    inner_features_source = [*conv4_3]
    # take out channenls from batchnorm num of features
    out_channel_list = [features[source].conv[1][1].num_features for source in inner_features_source]
    # done with the feature extractor
    out_channel_list.append(last_fe_channel)
    # now the extras:
    extras_out_channel_list = [512, 256, 256, 64]

    out_channel_list.extend(extras_out_channel_list)

    is_v1 = PriorBoxBase.detect_prior_v1(cfg=cfg)
    if is_v1:
        adaptive = NEW_ASPECT_RATIO_KEY in cfg.keys()
        cfg, priors_per_scale = calculate_priors_per_scale_for_v1_design(cfg, backwards_compatible=(not adaptive))
    else:
        prior_design = cfg['prior_design']
        priors_per_scale = PriorBoxBase.calculate_priors_per_scale(prior_design)

    base_ = list(features.children())
    extras_ = extras
    head_ = multibox_detection_branches_mobilenet(out_channel_list=out_channel_list,
                                                  priors_per_scale=priors_per_scale, num_classes=num_classes)

    return MobilenetSSD(phase, size, base_, extras_, head_, num_classes=num_classes, cfg=cfg, conv4_3=conv4_3,
                        dropout=dropout, logger=logger)



